import os

def render(state):
    facts = state.get_facts()
    height = {}
    max_x = 0
    max_y = 0
    for member in facts['height']:
        pos = member[0]
        h = member[1]
        x = int(pos[4])
        max_x = max(max_x, x)
        y = int(pos[6])
        max_y = max(max_y, y)
        heig = int(h[1])
        height[(x,y)] = heig
    
    at = facts['at'][0][0]
    robot_at = (int(at[4]), int(at[6]))
    
    grid = ""
    for y in range(max_y + 1):
        if y > 0:
            grid += os.linesep
        for x in range(max_x + 1):
            grid += str(height[(x,y)])
            if robot_at == (x, y):
                grid += ('R' if ('has_block' in facts) else 'r') + ' '
            else:
                grid += '  '
    return grid
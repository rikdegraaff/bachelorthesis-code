#ifndef UTILS_STRINGS_H
#define UTILS_STRINGS_H

#include "exceptions.h"

#include <sstream>
#include <string>

namespace utils {
class StringOperationError : public utils::Exception {
    std::string msg;
public:
    explicit StringOperationError(const std::string &msg);

    virtual void print() const override;
};

extern void lstrip(std::string &s);
extern void rstrip(std::string &s);
extern void strip(std::string &s);

/*
  Split a given string at the first occurrence of separator or throw
  StringOperationError if separator is not found.
*/
extern std::pair<std::string, std::string> split(
    const std::string &s, const std::string &separator);

template <class Collection>
void split_all(const std::string &str, Collection &collection, char separator = ' ') {
    size_t current, previous = 0;
    current = str.find(separator);
    while (current != std::string::npos) {
        collection.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(separator, previous);
    }
    collection.push_back(str.substr(previous, current - previous));
}

extern bool startswith(const std::string &s, const std::string &prefix);

template<typename Collection>
std::string join(const Collection &collection, const std::string &delimiter) {
    std::ostringstream oss;
    bool first_item = true;

    for (const auto &item : collection) {
        if (first_item)
            first_item = false;
        else
            oss << delimiter;
        oss << item;
    }
    return oss.str();
}

extern void replace_occurences(std::string& s, char to_replace, char replacement);

}
#endif

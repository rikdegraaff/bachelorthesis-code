#include <algorithm>
#include <iostream>
#include <string>

#include "scanner.h"

using namespace std;

namespace concepts {

bool Scanner::is_at_end()
{
	return current >= source.size();
}

void Scanner::scan_token()
{
	char c = advance();

	switch (c) {
	case '(': add_token(TokenType::LEFT_PAREN); break;
	case ')': add_token(TokenType::RIGHT_PAREN); break;
	case '{': add_token(TokenType::LEFT_BRACE); break;
	case '}': add_token(TokenType::RIGHT_BRACE); break;
	case '[': add_token(TokenType::LEFT_BRACKET); break;
	case ']': add_token(TokenType::RIGHT_BRACKET); break;
	case ',': add_token(TokenType::COMMA); break;
	case '.': add_token(TokenType::DOT); break;
	case '+': add_token(TokenType::PLUS); break;
	case '-':
		if (is_digit(peek())) {
			integer();
		} else {
			add_token(TokenType::MINUS);
		}
		break;
	case '>':
		if (peek() == '=') {
			advance();
			add_token(TokenType::AT_LEAST);
		} else {
			add_token(TokenType::MORE_THAN);
		}
		break;
	case '<':
		if (peek() == '=') {
			advance();
			add_token(TokenType::AT_MOST);
		} else {
			add_token(TokenType::LESS_THAN);
		}
		break;
	case '*': add_token(TokenType::STAR); break;
	case '#': add_token(TokenType::HASH); break;
	case ':': add_token(TokenType::COMPOSE); break;
	case '=': add_token(match('=') ? TokenType::EQUAL_EQUAL : TokenType::EQUAL); break;
	case '%': 
		while (peek() != '\n' && !is_at_end()) advance(); 
		col = 0; 
		break;
	case '\n': 
		line++;
		line_start = current;
		col = 0;
		break;
	case ' ':
	case '\t':
	case '\r':
		break; // ignore whitespace
	case '\0':
		break;
	default:
		if (is_identifier_starter(c)) {
			identifier();
		}
		else if (is_digit(c)) {
			integer();
		} else {
			error(line, col, string("Unexpected character: '") + c + "'");
		}
	}
}

std::string to_lwr(std::string s) {
	std::transform(s.begin(), s.end(), s.begin(),
		[](unsigned char c) { return std::tolower(c); });
	return s;
}

void Scanner::identifier() {
	while (is_identifier_char(peek()))
		advance();

	std::string text = source.substr(start, current - start);
	std::string lower_text = to_lwr(text);
	auto search = keywords.find(lower_text);
	TokenType type = 
		search != keywords.end() 
			? search->second
		: lower_text == "_"
			? TokenType::BOT
		: lower_text == "t"
			? TokenType::TOP
		: is_bound_variable(lower_text)
			? TokenType::BINDABLE_VARIABLE
			: TokenType::IDENTIFIER;
	add_token(type, lower_text);
}

bool Scanner::is_bound_variable(string &lower_text) {
	bool starts_correctly = lower_text[0] == 'x' || lower_text[0] == 'y' || lower_text[0] == 'z';
	if (!starts_correctly) {
		return false;
	}

	for (unsigned int i = 1; i < lower_text.size(); ++i) {
		if (!is_digit(lower_text[i])) {
			return false;
		}
	}

	return true;
}

void Scanner::integer() {
	while (is_digit(peek()))
		advance();
	std::string text = source.substr(start, current - start);
	add_token(TokenType::INTEGER, text);
}

bool Scanner::is_digit(char c) {
	return '0' <= c && c <= '9';
}

bool Scanner::is_identifier_starter(char c) {
	return ('a' <= c && c <= 'z')
		|| ('A' <= c && c <= 'Z')
		|| c == '_';
}

bool Scanner::is_identifier_char(char c) {
	return is_identifier_starter(c)
		|| is_digit(c)
		|| c == '\'';
}

bool Scanner::match(char expected) {
	if (is_at_end() || source[current] != expected)
		return false;

	current++;
	col++;

	return true;
}

char Scanner::peek() {
	return is_at_end()
		? '\0'
		: source[current];
}

char Scanner::peek_next() {
	return (current >= source.size())
		? '\0'
		: source[current];
}

char Scanner::advance() {
	col++;
	return source[current++];
}

void Scanner::add_token(TokenType type) {
	add_token(type, source.substr(start, current - start));
}

void Scanner::add_token(TokenType type, std::string lexeme) {
	tokens.emplace_back(type, lexeme, line, start);
}

void Scanner::error(int line, int col, std::string error) {
	throw ScannerError(error, source.substr(line_start, current - line_start), line, col);
}

std::list<Token> Scanner::scan_tokens() {
	while (!is_at_end()) {
		start = current;
		scan_token();
	}

	tokens.emplace_back(TokenType::END_OF_FILE, "", line, col);
	return tokens;
}

void ScannerError::print() const {
	cerr << "scanning error: " << msg << endl
		<< "  at: line " << line << ", col " << col << endl;
	if (!substring.empty()) {
		cerr << "  context: " << substring << endl;
	}
}

std::string token_string(TokenType t) {
	switch (t) {
	case TokenType::LEFT_PAREN: return "(";
	case TokenType::RIGHT_PAREN: return ")";
	case TokenType::LEFT_BRACE: return "{";
	case TokenType::RIGHT_BRACE: return "}";
	case TokenType::LEFT_BRACKET: return "[";
	case TokenType::RIGHT_BRACKET: return "]";
	case TokenType::COMMA: return ",";
	case TokenType::DOT: return ".";
	case TokenType::EQUAL: return "=";
	case TokenType::PLUS: return "+";
	case TokenType::MINUS: return "-";
	case TokenType::STAR: return "*";
	case TokenType::HASH: return "#";
	case TokenType::EQUAL_EQUAL: return "==";
	case TokenType::COMPOSE: return ":";
	case TokenType::TOP: return "top";
	case TokenType::BOT: return "bot";
	case TokenType::NOT: return "not";
	case TokenType::OR: return "or";
	case TokenType::AND: return "and";
	case TokenType::SOME: return "some";
	case TokenType::ALL: return "all";
	case TokenType::EXACTLY: return "exactly";
	case TokenType::AT_LEAST: return ">=";
	case TokenType::AT_MOST: return "<=";
	case TokenType::LESS_THAN: return "<";
	case TokenType::MORE_THAN: return ">";
	case TokenType::ARE: return "are";
	case TokenType::DIST: return "dist";
	case TokenType::RETURN: return "return";
	case TokenType::IDENTIFIER: return "<id>";
	case TokenType::BINDABLE_VARIABLE: return "<var>";
	case TokenType::INTEGER: return "<int>";
	case TokenType::END_OF_FILE: return "<eof>";
	default: return "<unknown token>";
	}
}

}
#ifndef CONCEPTS_TYPE_SYSTEM_H
#define CONCEPTS_TYPE_SYSTEM_H

#include <functional>
#include <set>
#include <vector>

#include "scanner.h"

namespace concepts {

class EvaluationError : public utils::Exception {
	const std::string msg;
public:
	EvaluationError(const std::string &error)
		: msg(error) {}

	virtual void print() const override;
};

class Value {
public:
	virtual ~Value() {}
	virtual void print(const std::vector<std::string>& individuals) = 0;
};

class Individual : public Value {
public:
	const int index;

	Individual(int index) : index(index) {}

	virtual void print(const std::vector<std::string>& individuals);
};

class ConceptModel : public Value {
public:
	std::vector<bool> members;

	ConceptModel(unsigned int size, bool def=false) : members(size, def) {}
	ConceptModel(std::vector<bool> &members) : members(members) {}

	virtual void print(const std::vector<std::string>& individuals);
};

class RoleModel : public Value {
public:
	std::vector<std::set<int>> members;

	RoleModel(unsigned int size) : members(size) {}
	RoleModel(std::vector<std::set<int>> members) : members(members) {}

	virtual void print(const std::vector<std::string> &individuals);
};

template<unsigned int arity>
class PredicateModel : public Value {
public:
	std::vector<std::vector<int>> members;

	PredicateModel() {}
	PredicateModel(std::vector<std::vector<int>> members) : members(members) {}

	virtual void print(const std::vector<std::string>& individuals) {
		std::cout << "{";
		for (auto member : members) {
			std::cout << "(";
			for (auto i : member) {
				std::cout << individuals[i] << ",";
			}
			std::cout << "),";
		}
		std::cout << "}";
	}
};

class Number : public Value {
public:
	const int number;

	Number(int number) : number(number) {}

	virtual void print(const std::vector<std::string> &individuals);
};

class NumberList : public Value {
public:
	std::vector<int> number_list;

	NumberList(int size) : number_list(size) {}

	virtual void print(const std::vector<std::string>& individuals);
};

enum class TypeEnum {
	INDIVIDUAL, CONCEPT, ROLE, PREDICATE, NUMBER, NUMBER_LIST, BINDABLE_VARIABLE
};

class Type {
	TypeEnum type;
	int number;
public:
	Type() : Type(TypeEnum::INDIVIDUAL) {}
	Type(const TypeEnum type, int number=0) : type(type), number(number) {}

	int get_number() {
		return number;
	}

	Type &operator=(TypeEnum v) { 
		type = v; 
		return *this;
	}

	Type &operator=(const Type v) { 
		type = v.type;
		number = v.number;
		return *this;
	}

	operator TypeEnum() const { return type; }
	bool operator==(const TypeEnum v) const { return type == v; }
	bool operator!=(const TypeEnum v) const { return type != v; }
};

std::string to_str(const concepts::Type& t);

void type_error(concepts::Type expected, concepts::Type actual);

class TypedValue {
	std::shared_ptr<concepts::Value> val;

public:
	concepts::Type type;
	/*
	TypedValue(concepts::Type type, concepts::Value &val) : type(type) {
		switch (type) {
		case TypeEnum::INDIVIDUAL:
			val.individual = val.individual;
			break;
		case TypeEnum::CONCEPT:
			val.concept_model = val.concept_model;
			break;
		case TypeEnum::ROLE:
			val.role_model = val.role_model;
			break;
		case TypeEnum::NUMBER:
			val.number = val.number;
			break;
		case TypeEnum::NUMBER_LIST:
			val.number_list = val.number_list;
			break;
		}
	}

	TypedValue(const TypedValue& other) : TypedValue(other.type, other.val) {}
	*/

	TypedValue() : type(TypeEnum::NUMBER) {}

	TypedValue(std::shared_ptr<Individual> individual);
	TypedValue(std::shared_ptr<ConceptModel> concept_model);
	TypedValue(std::shared_ptr<RoleModel> role_model);
	template<unsigned int arity>
	TypedValue(std::shared_ptr<PredicateModel<arity>> predicate_model) : type(TypeEnum::PREDICATE, arity) {
		val = predicate_model;
	}
	TypedValue(std::shared_ptr<Number> number);
	TypedValue(std::shared_ptr<NumberList> number_list);
	
	void print(const std::vector<std::string> &individuals);

	TypedValue& operator=(const TypedValue& v) { 
		type = v.type; 
		val = v.val; 
		return *this; 
	}

	std::shared_ptr<Individual> get_individual() {
		if (type != TypeEnum::INDIVIDUAL) {
			type_error(TypeEnum::INDIVIDUAL, type);
		}
		return std::static_pointer_cast<Individual>(val);
	}

	std::shared_ptr<ConceptModel> get_concept_model() {
		if (type != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, type);
		}
		return std::static_pointer_cast<ConceptModel>(val);
	}

	std::shared_ptr<RoleModel> get_role_model() {
		if (type != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, type);
		}
		return std::static_pointer_cast<RoleModel>(val);
	}

	template<unsigned int arity>
	std::shared_ptr<PredicateModel<arity>> get_predicate_model() {
		if (type != TypeEnum::PREDICATE || type.get_number() != arity) {
			type_error(TypeEnum::PREDICATE, type);
		}
		return std::static_pointer_cast<PredicateModel<arity>>(val);
	}

	std::shared_ptr<Number> get_number() {
		if (type != TypeEnum::NUMBER) {
			type_error(TypeEnum::NUMBER, type);
		}
		return std::static_pointer_cast<Number>(val);
	}

	std::shared_ptr<NumberList> get_number_list() {
		if (type != TypeEnum::NUMBER_LIST) {
			type_error(TypeEnum::NUMBER_LIST, type);
		}
		return std::static_pointer_cast<NumberList>(val);
	}
};

class Interpretation {
public:
	std::unordered_map<std::string, TypedValue> constants;
	const int universe_size;

	Interpretation(int universe_size) : universe_size(universe_size) {}

	Interpretation(std::unordered_map<std::string, TypedValue>&& constants, int universe_size)
		: constants(move(constants)), universe_size(universe_size) {}
};

class Operator {
public:
	const concepts::TokenType token;

	Operator(concepts::TokenType token) : token(token) {}

	virtual ~Operator() {}
};

class BinaryOperator : public Operator {
public:
	BinaryOperator(concepts::TokenType token) : Operator(token) {}

	virtual ~BinaryOperator() {}

	virtual Type *get_type(concepts::Type &left_input, concepts::Type &right_input) = 0;
	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) = 0;
};

class TernaryOperator : public Operator {
public:
	const TokenType right_token;
	TernaryOperator(TokenType token, TokenType right_token) : Operator(token), right_token(right_token) {}

	virtual ~TernaryOperator() {}

	virtual concepts::Type *get_type(concepts::Type &left_input, concepts::Type &middle_input, concepts::Type &right_input) = 0;
	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &middle_input, TypedValue &right_input) = 0;
};

class UnaryOperator : public Operator {
public:
	UnaryOperator(TokenType token) : Operator(token) {}

	virtual ~UnaryOperator() {}

	virtual concepts::Type *get_type(concepts::Type &input) = 0;
	virtual TypedValue *get_value(TypedValue &input) = 0;
};

class Function {
public:
	const TokenType token;
	const int arity;

	Function(TokenType token, int arity)
		: token(token), arity(arity) {}

	virtual ~Function() {}

	virtual Type *get_type(std::vector<concepts::Type> &inputs) = 0;
	virtual TypedValue *get_value(std::vector<TypedValue> &inputs) = 0;
};

class Subscript {
public:
	Subscript() {}

	virtual ~Subscript() {}

	virtual Type *get_type(Type &base, std::vector<Type> &inputs) = 0;
	virtual TypedValue *get_value(TypedValue &base, std::vector<TypedValue> &inputs, int universe_size) = 0;
};

class LiteralType {
public:
	const concepts::TokenType token;
	const concepts::Type type;

	LiteralType(concepts::TokenType token, concepts::Type type)
		: token(token), type(type) {}

	virtual ~LiteralType() {}

	virtual TypedValue *value(const Interpretation &interpretation, const Token &token) = 0;
};

class CollectionType {
public:
	const concepts::TokenType left_delimitor, right_delimitor;

	CollectionType(concepts::TokenType left, concepts::TokenType right)
		: left_delimitor(left), right_delimitor(right) {}

	virtual ~CollectionType() {}

	virtual concepts::Type *get_type(concepts::Type &input) = 0;
	virtual TypedValue *get_value(std::vector<TypedValue> &inputs, const Interpretation &interpretation) = 0;
};

class BindingType : public Operator {
public:
	const TokenType right_token = TokenType::ARE;

	BindingType(TokenType token) : Operator(token) {}

	virtual ~BindingType() {}

	virtual Type *get_type(Type &left_input, Type &right_input) = 0;
	virtual TypedValue *get_value(std::function<TypedValue *(TypedValue &)> value_for_assignment, TypedValue &right_input, int universe_size) = 0;
};

}

#endif
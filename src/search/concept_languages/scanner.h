#ifndef CONCEPTS_SCANNER_H
#define CONCEPTS_SCANNER_H

#include <list>
#include <string>
#include <unordered_map>

#include "../utils/exceptions.h"

namespace concepts {

enum class TokenType {
	// single-character
	LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE, LEFT_BRACKET, RIGHT_BRACKET,
	COMMA, DOT, EQUAL, PLUS, MINUS, STAR, HASH, COMPOSE,

	EQUAL_EQUAL,

	IDENTIFIER, BINDABLE_VARIABLE, INTEGER,

	// keywords
	TOP, BOT, NOT, OR, AND, SOME, ALL, EXACTLY, AT_LEAST, AT_MOST, LESS_THAN, MORE_THAN, ARE, DIST, RETURN,

	END_OF_FILE
};

std::string token_string(TokenType t);

const std::unordered_map<std::string, TokenType> keywords({
	{"top", TokenType::TOP},
	{"bot", TokenType::BOT},
	{"not", TokenType::NOT},
	{"or", TokenType::OR},
	{"and", TokenType::AND},
	{"some", TokenType::SOME},
	{"all", TokenType::ALL},
	{"exactly", TokenType::EXACTLY},
	{"at_least", TokenType::AT_LEAST},
	{"at_most", TokenType::AT_MOST},
	{"less_than", TokenType::LESS_THAN},
	{"more_than", TokenType::MORE_THAN},
	{"are", TokenType::ARE},
	{"dist", TokenType::DIST},
	{"return", TokenType::RETURN}
});

class ScannerError : public utils::Exception {
	const std::string msg;
	const std::string substring;
	const int line, col;
public:
	ScannerError(const std::string& error, const std::string& substring, int line, int col)
		: msg(error), substring(substring), line(line), col(col) {}

	virtual void print() const override;
};

struct Token {
	const TokenType type;
	const std::string lexeme;
	const int line;
	const int col;

	Token(TokenType type, std::string lexeme, int line, int col)
		: type(type), lexeme(lexeme), line(line), col(col) {}
};

class Scanner {
	const std::string &source;
	std::list<Token> tokens;
	int start = 0;
	unsigned int current = 0;
	int line_start = 0;
	int line = 1;
	int col = 0;

public:
	Scanner(std::string &source) : source(source) {}
	std::list<Token> scan_tokens();

private:
	bool is_at_end();
	void scan_token();
	void identifier();
	bool is_bound_variable(std::string& lower_text);
	void integer();
	bool is_digit(char c);
	bool is_identifier_starter(char c);
	bool is_identifier_char(char c);
	bool match(char expected);
	char peek();
	char peek_next();
	char advance();
	void add_token(TokenType type);
	void add_token(TokenType type, std::string lexeme);
	void error(int line, int col, std::string error);
};

}

#endif
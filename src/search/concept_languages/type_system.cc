#include <iostream>
#include <memory>

#include "type_system.h"

using namespace std;

namespace concepts {

TypedValue::TypedValue(shared_ptr<Individual> individual) : type(TypeEnum::INDIVIDUAL) {
	val = individual;
}

TypedValue::TypedValue(shared_ptr<ConceptModel> concept_model) : type(TypeEnum::CONCEPT) {
	val = concept_model;
}

TypedValue::TypedValue(shared_ptr<RoleModel> role_model) : type(TypeEnum::ROLE) {
	val = role_model;
}
TypedValue::TypedValue(shared_ptr<Number> number) : type(TypeEnum::NUMBER) {
	val = number;
}

TypedValue::TypedValue(shared_ptr<NumberList> number_list) : type(TypeEnum::NUMBER_LIST) {
	val = number_list;
}

void TypedValue::print(const std::vector<std::string> &individuals) {
	val->print(individuals);
}

void EvaluationError::print() const {
	cerr << "concept evaluation error: " << msg << endl;
	/*	<< "  at: line " << line ", col " << col << endl;
	if (!substring.empty()) {
		cerr << "  context: " << substring << endl;
	}*/
}

std::string to_str(const concepts::Type& t) {
	switch (t) {
	case TypeEnum::INDIVIDUAL:
		return "Individual";
	case TypeEnum::CONCEPT:
		return "Concept";
	case TypeEnum::ROLE:
		return "Role";
	case TypeEnum::PREDICATE:
		return "n-ary Role";
	case TypeEnum::NUMBER:
		return "Number";
	case TypeEnum::NUMBER_LIST:
		return "Number List";
	case TypeEnum::BINDABLE_VARIABLE:
		return "Bindable Variable";
	default:
		return "Invalid Type";
	}
}

[[ noreturn ]] void type_error(concepts::Type expected, concepts::Type actual) {
	throw EvaluationError("Incompatible types. Expected " + to_str(expected) + " but got " + to_str(actual));
}

void Individual::print(const std::vector<std::string>& individuals) {
	cout << individuals[index];
}

void Number::print(const std::vector<std::string>&) {
	cout << number;
}

void NumberList::print(const std::vector<std::string>&) {
	cout << "[";
	for (auto num : number_list) {
		cout << num << ",";
	}
	cout << "]";
}

void ConceptModel::print(const std::vector<std::string>& individuals) {
	cout << "{";
	for (unsigned int i = 0; i < members.size(); ++i) {
		if (members[i]) {
			cout << individuals[i] << ",";
		}
	}
	cout << "}";
}

void RoleModel::print(const std::vector<std::string>& individuals) {
	cout << "{";
	for (unsigned int i = 0; i < members.size(); ++i) {
		for (auto j : members[i]) {
			cout << "(" << individuals[i] << "," << individuals[j] << ")" << ",";
		}
	}
	cout << "}";
}

}
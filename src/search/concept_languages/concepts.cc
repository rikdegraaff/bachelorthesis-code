#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <unordered_map>
#include <vector>

#include "concepts.h"
#include "parser.h"

#include "../option_parser.h"
#include "../plugin.h"
#include "../task_proxy.h"

#include "../utils/strings.h"

using namespace std;
using namespace utils;

namespace concepts {

class ExprTypeChecker : public TypedExprVisitor<concepts::Type> {
	const unordered_map<std::string, concepts::Type> context;

public:
	ExprTypeChecker(unordered_map<std::string, concepts::Type>&& context)
		: context(move(context)) {}

	concepts::Type* check(Expr& expr) {
		return (concepts::Type*) expr.accept(*this);
	}

	virtual concepts::Type* visit(TernaryExpr& expr) {
		auto left = base_visit(expr.left);
		auto middle = base_visit(expr.middle);
		auto right = base_visit(expr.right);

		auto type = expr.op->get_type(*left, *middle, *right);

		delete left;
		delete middle;
		delete right;

		return type;
	}

	virtual concepts::Type* visit(BinaryExpr& expr) {
		auto left = base_visit(expr.left);
		auto right = base_visit(expr.right);

		auto type = expr.op->get_type(*left, *right);

		delete left;
		delete right;

		return type;
	}

	virtual concepts::Type* visit(UnaryExpr& expr) {
		auto input = base_visit(expr.expr);

		auto type = expr.op->get_type(*input);

		delete input;

		return type;
	}

	virtual concepts::Type* visit(FunctionExpr& expr) {
		vector<concepts::Type> inputs(expr.args.size());
		vector<concepts::Type*> input_ptrs(expr.args.size());

		for (unsigned int i = 0; i < expr.args.size(); ++i) {
			input_ptrs[i] = base_visit(expr.args[i]);
			inputs[i] = *input_ptrs[i];
		}
		
		auto type = expr.fun->get_type(inputs);

		for (unsigned int i = 0; i < inputs.size(); ++i) {
			delete input_ptrs[i];
		}

		return type;
	}

	virtual Type* visit(SubscriptExpr& expr) {
		auto base = base_visit(expr.base);
		vector<Type> inputs(expr.args.size());
		vector<Type*> input_ptrs(expr.args.size());

		for (unsigned int i = 0; i < expr.args.size(); ++i) {
			input_ptrs[i] = base_visit(expr.args[i]);
			inputs[i] = *input_ptrs[i];
		}

		auto type = expr.subscript->get_type(*base, inputs);

		for (unsigned int i = 0; i < inputs.size(); ++i) {
			delete input_ptrs[i];
		}

		delete base;

		return type;
	}

	virtual concepts::Type* visit(IdentifierExpr& expr) {
		if (expr.bindable) {
			return new Type{ TypeEnum::INDIVIDUAL };
		}

		auto search = context.find(expr.name);
		if (search == context.end()) {
			throw ParseError("Unknown identifier " + expr.name);
		}

		return new concepts::Type{ search->second };
	}

	virtual concepts::Type* visit(LiteralExpr& expr) {
		return new concepts::Type{ expr.type->type };
	}

	virtual concepts::Type* visit(CollectionExpr& expr) {
		concepts::Type *first = nullptr;

		for (auto in : expr.exprs) {
			auto t = base_visit(in);
			if (first == nullptr) {
				first = t;
				continue;
			}
			
			if (*t != *first) {
				type_error(*first, *t);
			}

			delete t;
		}

		auto type = expr.type->get_type(*first);

		delete first;

		return type;
	}

	virtual Type *visit(BindingExpr &expr) {
		auto input = base_visit(expr.expr);
		auto right_input = base_visit(expr.right_expr);

		auto type = expr.type->get_type(*input, *right_input);

		delete input;
		delete right_input;

		return type;
	}
};

class ExprCacheMarker : public NoReturnExprVisitor {
	unordered_map<intptr_t, int> need_count;
public:
	set<intptr_t> get_cache_needed(std::shared_ptr<Expr> expr) {
		base_visit(expr);
		set<intptr_t> cache;
		for (auto pair : need_count) {
			if (pair.second > 1) {
				cache.insert(pair.first);
			}
		}
		return cache;
	}

	void base(std::shared_ptr<Expr> expr) {
		if (expr->free.size() > 0) {
			base_visit(expr);
			return;
		}

		if (expr->is_static || dynamic_cast<IdentifierExpr*>(expr.get()) ) {
			return;
		}

		auto ptr = reinterpret_cast<intptr_t>(expr.get());
		auto search = need_count.find(ptr);
		if (search == need_count.end()) {
			need_count[ptr] = 1;
			base_visit(expr);
			return;
		}
		need_count[ptr]++;
	}

	virtual void visit(TernaryExpr &expr) {
		base(expr.left);
		base(expr.middle);
		base(expr.right);
	}

	virtual void visit(BinaryExpr& expr) {
		base(expr.left);
		base(expr.right);
	}

	virtual void visit(UnaryExpr& expr) {
		base(expr.expr);
	}

	virtual void visit(FunctionExpr& expr) {
		for (auto e : expr.args) {
			base(e);
		}
	}

	virtual void visit(SubscriptExpr& expr) {
		base(expr.base);
		for (auto e : expr.args) {
			base(e);
		}
	}

	virtual void visit(IdentifierExpr &) {}

	virtual void visit(LiteralExpr &) {}

	virtual void visit(CollectionExpr& expr) {
		for (auto e : expr.exprs) {
			base(e);
		}
	}

	virtual void visit(BindingExpr& expr) {
		base(expr.expr);
		base(expr.right_expr);
	}
};

/*
int ConceptLanguageHeuristic::compute_heuristic(const GlobalState& global_state) {
	State state = convert_global_state(global_state);
	return compute_heuristic(state);
}
*/
int ConceptLanguageHeuristic::compute_heuristic(const State &state) {
	auto numbers = evaluate_state(state);
	
	//for miconic.cpt
	int h = 8 * universe_size - 3 * numbers[0] - 5 * numbers[1] + 2 * numbers[2] + 1 * numbers[3];
	if (print) {
		cout << numbers[0] << " " << numbers[1] << " " << numbers[2] << " " << numbers[3] << ": " << h << " " << endl;
	}
	//for logistics.cpt
	//int sum = 0;
	//int h = 0;
	//for (int i = 0; i < numbers.size(); ++i) {
		//cout << numbers[i] << " ";
		//h += (i + 1) * numbers[i];
		//sum += numbers[i];
	//}
	//cout << ": " << h << " sum: " << sum << endl;

	return h;
}

void ConceptLanguageHeuristic::add_evaluations(const State &state, std::vector<int> &evaluations) {
	auto numbers = evaluate_state(state);
	evaluations.insert(evaluations.end(), numbers.begin(), numbers.end());
}

void ConceptLanguageHeuristic::add_evaluator_names(std::vector<std::string> &names) {
	const auto& exprs = static_pointer_cast<CollectionExpr>(features)->exprs;
	names.reserve(names.size() + exprs.size());
	stringstream out;
	ExprPrinter printer(out, true);
	for (auto expr : exprs) {
		printer.print(*expr);
		names.push_back(out.str());
		out.str("");
	}
}

vector<int> ConceptLanguageHeuristic::evaluate_state(const State &state) {
	unordered_map<std::string, TypedValue> constants = statics;

	for (auto pair : concept_values) {
		constants[get<0>(pair.second)] = TypedValue(make_shared<ConceptModel>(universe_size, false));
	}
	for (auto pair : role_values) {
		constants[get<0>(pair.second)] = TypedValue(make_shared<RoleModel>(universe_size));
	}
	for (auto pair : predicate_values) {
		constants[get<0>(pair.second)] = TypedValue(make_shared<PredicateModel<3>>());
	}
	for (auto pair : nullaries) {
		constants[pair.second] = TypedValue(make_shared<ConceptModel>(universe_size));
	}

	for (unsigned int var = 0; var < state.size(); ++var) {
		int value = state.get_values()[var];

		auto concept_search = concept_values.find(make_pair(var, value));
		if (concept_search != concept_values.end()) {
			auto tuple = concept_search->second;
			auto constant_search = constants.find(get<0>(tuple));
			shared_ptr<ConceptModel> model = nullptr;
			if (constant_search == constants.end()) {
				model = make_shared<ConceptModel>(universe_size, false);
				constants[get<0>(tuple)] = TypedValue(model);
			}
			else {
				model = constant_search->second.get_concept_model();
			}
			model->members[get<1>(tuple)] = true;
			continue;
		}

		auto role_search = role_values.find(make_pair(var, value));
		if (role_search != role_values.end()) {
			auto tuple = role_search->second;
			auto constant_search = constants.find(get<0>(tuple));
			shared_ptr<RoleModel> model = nullptr;
			if (constant_search == constants.end()) {
				model = make_shared<RoleModel>(universe_size);
				constants[get<0>(tuple)] = TypedValue(model);
			}
			else {
				model = constant_search->second.get_role_model();
			}
			model->members[get<1>(tuple)].insert(get<2>(tuple));
		}

		auto nullary_search = nullaries.find(make_pair(var, value));
		if (nullary_search != nullaries.end()) {
			auto name = nullary_search->second;
			auto constant_search = constants.find(name);
			shared_ptr<ConceptModel> model = nullptr;
			if (constant_search == constants.end()) {
				model = make_shared<ConceptModel>(universe_size, true);
				constants[name] = TypedValue(model);
			}
			else {
				model = constant_search->second.get_concept_model();
				for (unsigned int i = 0; i < model->members.size(); ++i) {
					model->members[i] = true;
				}
			}
		}

		auto predicate_search = predicate_values.find(make_pair(var, value));
		if (predicate_search != predicate_values.end()) {
			auto tuple = predicate_search->second;
			int arity = get<1>(tuple).size();
			if (arity == 3) {
				auto constant_search = constants.find(get<0>(tuple));
				shared_ptr<PredicateModel<3>> model = nullptr;
				if (constant_search == constants.end()) {
					model = make_shared<PredicateModel<3>>();
					constants[get<0>(tuple)] = TypedValue(model);
				}
				else {
					model = constant_search->second.get_predicate_model<3>();
				}
				model->members.push_back(get<1>(tuple));
			} else if (arity == 4) {
				auto constant_search = constants.find(get<0>(tuple));
				shared_ptr<PredicateModel<4>> model = nullptr;
				if (constant_search == constants.end()) {
					model = make_shared<PredicateModel<4>>();
					constants[get<0>(tuple)] = TypedValue(model);
				}
				else {
					model = constant_search->second.get_predicate_model<4>();
				}
				model->members.push_back(get<1>(tuple));
			} else if (arity == 5) {
				auto constant_search = constants.find(get<0>(tuple));
				shared_ptr<PredicateModel<5>> model = nullptr;
				if (constant_search == constants.end()) {
					model = make_shared<PredicateModel<5>>();
					constants[get<0>(tuple)] = TypedValue(model);
				}
				else {
					model = constant_search->second.get_predicate_model<5>();
				}
				model->members.push_back(get<1>(tuple));
			}
			
		}
	}

	evaluator.set_constants(move(constants));

	auto res = evaluator.evaluate(*features);
	auto val = res->get_number_list()->number_list;
	delete res;
	return val;
}

ConceptLanguageHeuristic::ConceptLanguageHeuristic(options::Options &opts) 
	: MultiEvaluator(opts),
	features(new Expr(true, std::set<std::string>())), 
	identifiers(),
	statics(),
	concept_values(),
	role_values(),
	evaluator(initialize_evaluator(opts)) {}

ExprEvaluator ConceptLanguageHeuristic::initialize_evaluator(options::Options& opts) {
	ifstream static_information(opts.get<string>("static_information"));

	set<string> static_names;

	vector<string> parts; 
	string name;
	while (getline(static_information, name)) {
		if (name[0] != 'A') {
			continue;
		}
		parts.clear();
		replace_occurences(name, '(', ' ');
		replace_occurences(name, ')', ' ');
		name.erase(remove(name.begin(), name.end(), ','), name.end());
		replace_occurences(name, '-', '_');
		rstrip(name);
		split_all(name, parts);
		if (parts[0] != "Atom") {
			continue;
		}

		if (parts[1] == "object") {
			continue;
		}

		for (unsigned int i = 1; i < parts.size(); ++i) {
			static_names.insert(parts[i]);
		}
	}

	static_information.close();

	auto file = opts.get<string>("file");
	parse_features(file, static_names);

	print = opts.get<bool>("print");

	unordered_map<std::string, concepts::Type> context;

	vector<string> individual_names;
	int num_individuals = 0;

	for (auto op : task_proxy.get_operators()) {
		string name = op.get_name();
		parts.clear();
		replace_occurences(name, '-', '_');
		rstrip(name);
		split_all(name, parts);
		for (unsigned int i = 1; i < parts.size(); ++i) {
			context[parts[i]] = TypeEnum::INDIVIDUAL;
			auto search = statics.find(parts[i]);
			if (search == statics.end()) {
				individual_names.push_back(parts[i]);
				statics[parts[i]] = TypedValue(make_shared<Individual>(num_individuals++));
			}
		}
	}

	vector<int> individuals;
	for (auto var : task_proxy.get_variables()) {
		for (int value = 0; value < var.get_domain_size(); ++value) {
			string name = var.get_fact(value).get_name();
			if (name[0] != 'A') {
				continue;
			}
			parts.clear();
			individuals.clear();
			replace_occurences(name, '(', ' ');
			replace_occurences(name, ')', ' ');
			name.erase(remove(name.begin(), name.end(), ','), name.end());
			replace_occurences(name, '-', '_');
			rstrip(name);
			split_all(name, parts);
			if (parts[0] != "Atom") {
				continue;
			}

			for (unsigned int i = 2; i < parts.size(); ++i) {
				context[parts[i]] = TypeEnum::INDIVIDUAL;
				auto search = statics.find(parts[i]);
				if (search == statics.end()) {
					individual_names.push_back(parts[i]);
					individuals.push_back(num_individuals);
					statics[parts[i]] = TypedValue(make_shared<Individual>(num_individuals++));
				} else {
					individuals.push_back(search->second.get_individual()->index);
				}
			}

			if (parts.size() == 2) {
				if (identifiers.find(parts[1]) != identifiers.end()) {
					context[parts[1]] = TypeEnum::CONCEPT;
					nullaries[make_pair(var.get_id(), value)] = parts[1];
				}
			}
			else if (parts.size() == 3) {
				bool used = false;
				if (identifiers.find(parts[1]) != identifiers.end()) {
					used = true;
					context[parts[1]] = TypeEnum::CONCEPT;
				}
				if (identifiers.find(parts[1] + "_g") != identifiers.end()) {
					used = true;
					context[parts[1] + "_g"] = TypeEnum::CONCEPT;
				}
				if (used) {
					concept_values[make_pair(var.get_id(), value)] = make_tuple(parts[1], individuals[0]);
				}
			} else if (parts.size() == 4) {
				bool used = false;
				if (identifiers.find(parts[1]) != identifiers.end()) {
					used = true;
					context[parts[1]] = TypeEnum::ROLE;
				}
				if (identifiers.find(parts[1] + "_g") != identifiers.end()) {
					used = true;
					context[parts[1] + "_g"] = TypeEnum::ROLE;
				}
				if (used) {
					role_values[make_pair(var.get_id(), value)] = make_tuple(parts[1], individuals[0], individuals[1]);
				}
			} else if (parts.size() > 4) {
				bool used = false;
				if (identifiers.find(parts[1]) != identifiers.end()) {
					used = true;
					context[parts[1]] = Type(TypeEnum::PREDICATE, parts.size() - 2);
				}
				if (identifiers.find(parts[1] + "_g") != identifiers.end()) {
					used = true;
					context[parts[1] + "_g"] = Type(TypeEnum::PREDICATE, parts.size() - 2);
				}
				if (used) {
					predicate_values[make_pair(var.get_id(), value)] = make_tuple(parts[1], individuals);
				}
			}
		}
	}

	universe_size = num_individuals;

	for (auto pair : concept_values) {
		if (identifiers.find(get<0>(pair.second) + "_g") != identifiers.end()) {
			statics[get<0>(pair.second) + "_g"] = TypedValue(make_shared<ConceptModel>(universe_size, false));
		}
	}
	for (auto pair : role_values) {
		if (identifiers.find(get<0>(pair.second) + "_g") != identifiers.end()) {
			statics[get<0>(pair.second) + "_g"] = TypedValue(make_shared<RoleModel>(universe_size));
		}
	}
	for (auto pair : predicate_values) {
		if (identifiers.find(get<0>(pair.second) + "_g") != identifiers.end()) {
			statics[get<0>(pair.second) + "_g"] = TypedValue(make_shared<PredicateModel<3>>());
		}
	}


	static_information = ifstream(opts.get<string>("static_information"));

	while (getline(static_information, name)) {
		if (name[0] != 'A') {
			continue;
		}
		parts.clear();
		individuals.clear();
		replace_occurences(name, '(', ' ');
		replace_occurences(name, ')', ' ');
		name.erase(remove(name.begin(), name.end(), ','), name.end());
		replace_occurences(name, '-', '_');
		rstrip(name);
		split_all(name, parts);
		if (parts[0] != "Atom") {
			continue;
		}

		if (parts[1] == "object") {
			continue;
		}

		bool cont = false;
		for (unsigned int i = 2; i < parts.size(); ++i) {
			context[parts[i]] = TypeEnum::INDIVIDUAL;
			auto search = statics.find(parts[i]);
			if (search == statics.end()) {
				cont = true;
				continue;
			} else {
				individuals.push_back(search->second.get_individual()->index);
			}
		}
		if (cont) {
			continue;
		}

		int arity = parts.size() - 2;

		auto search = statics.find(parts[1]);
		if (search == statics.end()) {
			switch (arity) {
			case 1:
				context[parts[1]] = TypeEnum::CONCEPT;
				statics[parts[1]] = TypedValue(make_shared<ConceptModel>(universe_size));
				break;
			case 2:
				context[parts[1]] = TypeEnum::ROLE;
				statics[parts[1]] = TypedValue(make_shared<RoleModel>(universe_size));
				break;
			case 3:
				context[parts[1]] = Type(TypeEnum::PREDICATE, 3);
				statics[parts[1]] = TypedValue(make_shared<PredicateModel<3>>());
				break;
			case 4:
				context[parts[1]] = Type(TypeEnum::PREDICATE, 4);
				statics[parts[1]] = TypedValue(make_shared<PredicateModel<4>>());
				break;
			case 5:
				context[parts[1]] = Type(TypeEnum::PREDICATE, 5);
				statics[parts[1]] = TypedValue(make_shared<PredicateModel<5>>());
				break;
			}
		}

		switch (arity) {
		case 1:
			statics[parts[1]].get_concept_model()->members[individuals[0]] = true;
			break;
		case 2:
			statics[parts[1]].get_role_model()->members[individuals[0]].insert(individuals[1]);
			break;
		case 3:
			statics[parts[1]].get_predicate_model<3>()->members.push_back(individuals);
			break;
		case 4:
			statics[parts[1]].get_predicate_model<4>()->members.push_back(individuals);
			break;
		case 5:
			statics[parts[1]].get_predicate_model<5>()->members.push_back(individuals);
			break;
		}
	}

	static_information.close();

	ExprTypeChecker checker(move(context));
	auto type = checker.check(*features);

	ExprCacheMarker marker;
	auto cache_needed = marker.get_cache_needed(features);

	if (*type != TypeEnum::NUMBER_LIST) {
		type_error(TypeEnum::NUMBER_LIST, *type);
	}

	for (auto goal : task_proxy.get_goals()) {
		auto pair = goal.get_pair();

		auto concept_search = concept_values.find(make_pair(pair.var, pair.value));
		if (concept_search != concept_values.end()) {
			auto tuple = concept_search->second;
			if (identifiers.find(get<0>(tuple) + "_g") != identifiers.end()) {
				statics[get<0>(tuple) + "_g"].get_concept_model()->members[get<1>(tuple)] = true;
			}
			continue;
		}

		auto role_search = role_values.find(make_pair(pair.var, pair.value));
		if (role_search != role_values.end()) {
			auto tuple = role_search->second;
			if (identifiers.find(get<0>(tuple) + "_g") != identifiers.end()) {
				statics[get<0>(tuple) + "_g"].get_role_model()->members[get<1>(tuple)].insert(get<2>(tuple));
			}
		}
	}

	return ExprEvaluator(individual_names, print, universe_size, cache_needed);
}

void ConceptLanguageHeuristic::parse_features(string &file, set<string> &statics) {
	ifstream ifs(file);
	string source((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));

	vector<shared_ptr<BinaryOperator>> product_operators;
	product_operators.push_back(make_shared<Product>());
	auto product_level = make_shared<BinaryFixityLevel>(true, product_operators);

	vector<shared_ptr<UnaryOperator>> feature_prefix_operators;
	feature_prefix_operators.push_back(make_shared<Card>());
	auto feature_prefix_level = make_shared<UnaryFixityLevel>(true, feature_prefix_operators);

	vector<shared_ptr<BinaryOperator>> or_operators;
	or_operators.push_back(make_shared<Or>());
	auto or_level = make_shared<BinaryFixityLevel>(true, or_operators);

	vector<shared_ptr<BinaryOperator>> and_operators;
	and_operators.push_back(make_shared<And>());
	auto and_level = make_shared<BinaryFixityLevel>(true, and_operators);

	vector<shared_ptr<BinaryOperator>> equality_operators;
	equality_operators.push_back(make_shared<Equals>());
	auto equality_level = make_shared<BinaryFixityLevel>(true, equality_operators);

	vector<shared_ptr<BinaryOperator>> compose_operators;
	compose_operators.push_back(make_shared<Compose>());
	auto compose_level = make_shared<BinaryFixityLevel>(true, compose_operators);

	vector<shared_ptr<TernaryOperator>> cardinality_operators;
	cardinality_operators.push_back(make_shared<Exactly>());
	cardinality_operators.push_back(make_shared<AtLeast>());
	cardinality_operators.push_back(make_shared<AtMost>());
	cardinality_operators.push_back(make_shared<LessThan>());
	cardinality_operators.push_back(make_shared<MoreThan>());
	vector<shared_ptr<BindingType>> binding_operators;
	binding_operators.push_back(make_shared<BindingAll>());
	binding_operators.push_back(make_shared<BindingSome>());
	auto cardinality_level = make_shared<TernaryFixityLevel>(false, cardinality_operators, binding_operators);

	vector<shared_ptr<BinaryOperator>> quantifier_operators;
	quantifier_operators.push_back(make_shared<Exists>());
	quantifier_operators.push_back(make_shared<All>());
	auto quantifier_level = make_shared<BinaryFixityLevel>(false, quantifier_operators);

	vector<shared_ptr<UnaryOperator>> prefix_operators;
	prefix_operators.push_back(make_shared<Not>());
	auto prefix_level = make_shared<UnaryFixityLevel>(true, prefix_operators);

	vector<shared_ptr<UnaryOperator>> suffix_operators;
	suffix_operators.push_back(make_shared<Inverse>());
	suffix_operators.push_back(make_shared<Closure>());
	auto suffix_level = make_shared<UnaryFixityLevel>(false, suffix_operators);

	vector<shared_ptr<FixityLevel>> levels(10);
	levels[0] = product_level;
	levels[1] = feature_prefix_level;
	levels[2] = or_level;
	levels[3] = and_level;
	levels[4] = equality_level;
	levels[5] = compose_level;
	levels[6] = cardinality_level;
	levels[7] = quantifier_level;
	levels[8] = prefix_level;
	levels[9] = suffix_level;

	vector<shared_ptr<CollectionType>> collection_types(1);
	collection_types[0] = make_shared<Set>();

	vector<shared_ptr<LiteralType>> literal_types(3);
	literal_types[0] = make_shared<Top>();
	literal_types[1] = make_shared<Bot>();
	literal_types[2] = make_shared<Integer>();

	vector<shared_ptr<Function>> functions(1);
	functions[0] = make_shared<Dist>();

	auto restriction = make_shared<PartialRestriction>();

	Scanner scanner(source);
	auto tokens = scanner.scan_tokens();

	ConceptParser parser(move(levels), move(collection_types), move(literal_types), move(functions), restriction, statics);

	features = parser.parse(tokens);
	identifiers = parser.get_identifiers();

	//ExprPrinter printer;
	//printer.print(*features);
}

static shared_ptr<MultiEvaluator> _parse(OptionParser& parser) {
	parser.document_synopsis("Concept Language heuristic", "");
	parser.document_language_support("action costs", "supported");
	parser.document_language_support("conditional effects", "supported");
	parser.document_language_support(
		"axioms",
		"supported (in the sense that the planner won't complain -- ");
	parser.document_property("admissible", "no");
	parser.document_property("consistent", "no");
	parser.document_property("safe", "no");
	parser.document_property("preferred operators", "no");

	parser.add_option<string>("file",
		"the file containing the definition of the features to be used");

	parser.add_option<bool>("print",
		"set to true if intermediately computed concepts and value should be printed",
		"false");
	parser.add_option<string>("static_information", 
		"the file containing static information for this task", 
		"static-information.out");

	MultiEvaluator::add_options_to_parser(parser);
	Options opts = parser.parse();
	if (parser.dry_run())
		return nullptr;
	else
		return make_shared<ConceptLanguageHeuristic>(opts);
}

static Plugin<MultiEvaluator> _plugin("concepts", _parse);

ExprEvaluator::ExprEvaluator(vector<string> individuals, bool print, int universe_size, set<intptr_t> cache_needed)
	: individuals(individuals), print(print), interpretation(universe_size), cache_needed(cache_needed) {}

void ExprEvaluator::set_constants(unordered_map<std::string, TypedValue>&& constants) {
	interpretation.constants = move(constants);
}

TypedValue* ExprEvaluator::evaluate(Expr& expr) {
	cache.clear();
	return (TypedValue*)expr.accept(*this);
}

TypedValue *ExprEvaluator::cached_evaluate(std::shared_ptr<Expr> expr) {
	auto ptr = reinterpret_cast<intptr_t>(expr.get());
	if (use_static_cache && expr->is_static && expr->free.empty()) {
		auto search = static_cache.find(ptr);
		if (search == static_cache.end()) {
			use_static_cache = false;
			auto res = base_visit(expr);
			use_static_cache = true;
			static_cache[ptr] = *res;
			return res;
		}
		return new TypedValue{ search->second };
	}

	if (cache_needed.find(ptr) != cache_needed.end()) {
		auto search = cache.find(ptr);
		if (search == cache.end()) {
			auto res = base_visit(expr);
			cache[ptr] = *res;
			return res;
		}
		return new TypedValue{ search->second };
	}

	return base_visit(expr);
}

TypedValue* ExprEvaluator::visit(TernaryExpr& expr) {
	auto left = cached_evaluate(expr.left);
	auto middle = cached_evaluate(expr.middle);
	auto right = cached_evaluate(expr.right);

	auto value = expr.op->get_value(*left, *middle, *right);

	delete left;
	delete middle;
	delete right;

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

TypedValue* ExprEvaluator::visit(BinaryExpr& expr) {
	auto left = cached_evaluate(expr.left);
	auto right = cached_evaluate(expr.right);

	auto value = expr.op->get_value(*left, *right);

	delete left;
	delete right;

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

inline TypedValue* ExprEvaluator::visit(UnaryExpr& expr) {
	auto input = cached_evaluate(expr.expr);

	auto value = expr.op->get_value(*input);

	delete input;

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

inline TypedValue* ExprEvaluator::visit(FunctionExpr& expr) {
	vector<TypedValue> inputs(expr.args.size());
	vector<TypedValue*> input_ptrs(expr.args.size());

	for (unsigned int i = 0; i < expr.args.size(); ++i) {
		input_ptrs[i] = cached_evaluate(expr.args[i]);
		inputs[i] = *input_ptrs[i];
	}

	auto value = expr.fun->get_value(inputs);

	for (unsigned int i = 0; i < inputs.size(); ++i) {
		delete input_ptrs[i];
	}

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

TypedValue* ExprEvaluator::visit(SubscriptExpr& expr) {
	auto base = cached_evaluate(expr.base);
	vector<TypedValue> inputs(expr.args.size());
	vector<TypedValue*> input_ptrs(expr.args.size());

	for (unsigned int i = 0; i < expr.args.size(); ++i) {
		input_ptrs[i] = cached_evaluate(expr.args[i]);
		inputs[i] = *input_ptrs[i];
	}

	auto value = expr.subscript->get_value(*base, inputs, interpretation.universe_size);

	for (unsigned int i = 0; i < inputs.size(); ++i) {
		delete input_ptrs[i];
	}

	delete base;

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

TypedValue* ExprEvaluator::visit(BindingExpr& expr) {
	auto right_value = cached_evaluate(expr.right_expr);
	auto value = expr.type->get_value([expr, this](TypedValue &value) {
		auto temp = interpretation.constants[expr.var];
		interpretation.constants[expr.var] = value;
		auto res = cached_evaluate(expr.expr);
		interpretation.constants[expr.var] = temp;
		return res;
	}, *right_value, interpretation.universe_size);

	delete right_value;

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

inline TypedValue* ExprEvaluator::visit(IdentifierExpr& expr) {
	auto search = interpretation.constants.find(expr.name);
	if (search == interpretation.constants.end()) {
		throw EvaluationError("Unknown identifier " + expr.name);
	}

	auto value = new TypedValue{ search->second };

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

inline TypedValue* ExprEvaluator::visit(LiteralExpr& expr) {
	return expr.type->value(interpretation, expr.token);
}

inline TypedValue* ExprEvaluator::visit(CollectionExpr& expr) {
	vector<TypedValue> inputs(expr.exprs.size());
	vector<TypedValue*> input_ptrs(expr.exprs.size());

	int i = 0;
	for (auto e : expr.exprs) {
		input_ptrs[i] = cached_evaluate(e);
		inputs[i] = *input_ptrs[i];
		++i;
	}

	auto value = expr.type->get_value(inputs, interpretation);

	for (unsigned int i = 0; i < inputs.size(); ++i) {
		delete input_ptrs[i];
	}

	if (print) {
		printer.print(expr);
		value->print(individuals);
		cout << endl;
	}

	return value;
}

void ExprPrinter::base(std::shared_ptr<Expr> expr) {
	if (use_aliases && expr->has_alias) {
		out << expr->alias;
	} else {
		base_visit(expr);
	}
}

void ExprPrinter::print(Expr& expr) {
	expr.accept(*this);
}

void ExprPrinter::visit(TernaryExpr& expr) {
	out << "(" << token_string(expr.op->token) << " " << token_string(expr.op->right_token) << " ";
	base(expr.left);
	out << " ";
	base(expr.middle);
	out << " ";
	base(expr.right);
	out << ")";
}

void ExprPrinter::visit(BinaryExpr& expr) {
	out << "(" << token_string(expr.op->token) << " ";
	base(expr.left);
	out << " ";
	base(expr.right);
	out << ")";
}

void ExprPrinter::visit(UnaryExpr& expr) {
	out << "(" << token_string(expr.op->token) << " ";
	base(expr.expr);
	out << ")";
}

void ExprPrinter::visit(FunctionExpr& expr) {
	out << token_string(expr.fun->token) << "(";
	unsigned int i = 0;
	for (auto e : expr.args) {
		base(e);
		if ((++i) < expr.args.size()) {
			out << ", ";
		}
	}
	out << ")";
}

void ExprPrinter::visit(SubscriptExpr& expr) {
	out << "(sub ";
	base(expr.base);
	out << " ";
	unsigned int i = 0;
	for (auto e : expr.args) {
		base(e);
		if ((++i) < expr.args.size()) {
			out << " ";
		}
	}
	out << ")";
}

void ExprPrinter::visit(IdentifierExpr& expr) {
	out << expr.name;
}

void ExprPrinter::visit(LiteralExpr& expr) {
	out << expr.token.lexeme;
}

void ExprPrinter::visit(CollectionExpr& expr) {
	out << token_string(expr.type->left_delimitor);

	unsigned int i = 0;
	for (auto e : expr.exprs) {
		base_visit(e);
		if ((++i) < expr.exprs.size()) {
			out << ", ";
		}
	}

	out << token_string(expr.type->right_delimitor);
}

void ExprPrinter::visit(BindingExpr& expr) {
	out << "(" << token_string(expr.type->token) << " " << token_string(expr.type->right_token) << " bind " << expr.var << " to ";
	base_visit(expr.expr);
	out << " ";
	base_visit(expr.right_expr);
	out << ")";
}

}
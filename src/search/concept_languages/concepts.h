#ifndef CONCEPTS_CONCEPTS_H
#define CONCEPTS_CONCEPTS_H

#include <algorithm>
#include <string>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <vector>

#include "../heuristic.h"
#include "../utils/exceptions.h"
#include "../utils/hash.h"
#include "../evaluators/multi_evaluator.h"

#include "parser.h"
#include "scanner.h"
#include "type_system.h"

#include <iostream>

namespace concepts {

class ExprPrinter : public NoReturnExprVisitor {
	std::ostream& out;
	bool use_aliases;
public:
	ExprPrinter(std::ostream& out=std::cout, bool use_aliases=false) : out(out), use_aliases(use_aliases) {}

	void base(std::shared_ptr<Expr> expr);

	void print(Expr& expr);

	virtual void visit(TernaryExpr& expr);

	virtual void visit(BinaryExpr& expr);

	virtual void visit(UnaryExpr& expr);

	virtual void visit(FunctionExpr& expr);

	virtual void visit(SubscriptExpr& expr);

	virtual void visit(IdentifierExpr& expr);

	virtual void visit(LiteralExpr& expr);

	virtual void visit(CollectionExpr& expr);

	virtual void visit(BindingExpr& expr);
};

class ExprEvaluator : public TypedExprVisitor<TypedValue> {
	const std::vector<std::string> individuals;
	std::unordered_map<intptr_t, TypedValue> static_cache;
	std::unordered_map<intptr_t, TypedValue> cache;
	bool use_static_cache = true;
	bool print;
	ExprPrinter printer;
	Interpretation interpretation;
	std::set<intptr_t> cache_needed;

public:
	ExprEvaluator(std::vector<std::string> individuals, bool print, int universe_size, std::set<intptr_t> cache_needed);

	void set_constants(std::unordered_map<std::string, TypedValue>&& constants);

	TypedValue* evaluate(Expr& expr);

	TypedValue *cached_evaluate(std::shared_ptr<Expr> expr);

	virtual TypedValue* visit(TernaryExpr& expr);

	virtual TypedValue* visit(BinaryExpr& expr);

	virtual TypedValue* visit(UnaryExpr& expr);

	virtual TypedValue* visit(FunctionExpr& expr);

	virtual TypedValue* visit(SubscriptExpr& expr);

	virtual TypedValue* visit(IdentifierExpr& expr);

	virtual TypedValue* visit(LiteralExpr& expr);

	virtual TypedValue* visit(CollectionExpr& expr);

	virtual TypedValue* visit(BindingExpr& expr);
};

class ConceptLanguageHeuristic : public MultiEvaluator {
	bool print;

	std::shared_ptr<Expr> features;
	std::set<std::string> identifiers;
	
	int universe_size;

	std::unordered_map<std::string, TypedValue> statics;
	utils::HashMap<std::pair<int, int>, std::tuple<std::string, int>> concept_values;
	utils::HashMap<std::pair<int, int>, std::tuple<std::string, int, int>> role_values;
	utils::HashMap<std::pair<int, int>, std::tuple<std::string, std::vector<int>>> predicate_values;
	utils::HashMap<std::pair<int, int>, std::string> nullaries;

	ExprEvaluator evaluator;

	//virtual int compute_heuristic(const GlobalState& global_state) override;
	int compute_heuristic(const State& state);
	std::vector<int> evaluate_state(const State& state);
	ExprEvaluator initialize_evaluator(options::Options& opts);
public:
	explicit ConceptLanguageHeuristic(options::Options& opts);
	void parse_features(std::string &file, std::set<std::string> &statics);
	virtual void add_evaluations(const State &state, std::vector<int> &evaluations);
	virtual void add_evaluator_names(std::vector<std::string> &names);
};

class PartialRestriction : public Subscript {
public:
	PartialRestriction() {}

	virtual ~PartialRestriction() {}

	virtual Type *get_type(Type &base, std::vector<Type> &inputs) {
		if (base != TypeEnum::PREDICATE && base != TypeEnum::ROLE) {
			type_error(TypeEnum::PREDICATE, base);
		}

		unsigned int arity = base == TypeEnum::PREDICATE ? base.get_number() : 2;

		if (inputs.size() != arity) {
			throw EvaluationError("Arity of role " + std::to_string(arity) + " does not match number of arguments supplied " + std::to_string(inputs.size()));
		}

		for (auto input : inputs) {
			if (input == TypeEnum::CONCEPT) {
				continue;
			} else if (input == TypeEnum::INDIVIDUAL || input == TypeEnum::BINDABLE_VARIABLE) {
				--arity;
			} else {
				type_error(TypeEnum::INDIVIDUAL, input);
			}
		}

		if (arity == base.get_number()) {
			throw EvaluationError("Must use at least one non wildcard in subscript");
		}

		if (arity == 1) {
			return new Type(TypeEnum::CONCEPT);
		} else if (arity == 2) {
			return new Type(TypeEnum::ROLE);
		} else if (arity > 2) {
			return new Type(TypeEnum::PREDICATE, arity);
		} else {
			throw EvaluationError("Must use at least one wildcard in subscript");
		}
	}

	virtual TypedValue *get_value(TypedValue &base, std::vector<TypedValue> &inputs, int universe_size) {
		int arity = base.type == TypeEnum::PREDICATE ? base.type.get_number() : 2;
		std::vector<int> filter(arity, -1);

		int i = 0;
		for (auto input : inputs) {
			if (input.type == TypeEnum::INDIVIDUAL) {
				--arity;
				filter[i] = input.get_individual()->index;
			} else {

			}
			++i;
		}

		std::vector<std::vector<int>> members;
		if (base.type == TypeEnum::PREDICATE) {
			switch (base.type.get_number()) {
			case 5:
				members = get_members<5>(*base.get_predicate_model<5>(), inputs);
				break;
			case 4:
				members = get_members<4>(*base.get_predicate_model<4>(), inputs);
				break;
			case 3:
				members = get_members<3>(*base.get_predicate_model<3>(), inputs);
				break;
			}
		} else {
			auto mems = std::vector<std::vector<int>>();
			auto role_model = base.get_role_model()->members;
			for (unsigned int i = 0; i < role_model.size(); ++i) {
				for (auto j : role_model[i]) {
					std::vector<int> member = {i, j};
					mems.push_back(member);
				}
			}
			auto predicate_model = PredicateModel<2>(mems);
			members = get_members<2>(predicate_model, inputs);
		}

		if (arity == 1) {
			auto model = std::make_shared<ConceptModel>(universe_size, false);
			for (auto member : members) {
				model->members[member[0]] = true;
			}
			return new TypedValue(model);
		} else if (arity == 2) {
			auto model = std::make_shared<RoleModel>(universe_size);
			for (auto member : members) {
				model->members[member[0]].insert(member[1]);
			}
			return new TypedValue(model);
		} else {
			switch (arity) {
			case 4:
				return new TypedValue(std::make_shared<PredicateModel<4>>(members));
			case 3:
				return new TypedValue(std::make_shared<PredicateModel<3>>(members));
			}
		}
		return nullptr;
	}

private:
	template<unsigned int arity>
	std::vector<std::vector<int>> get_members(PredicateModel<arity> &model, std::vector<TypedValue> &filter) {
		auto members = model.members;
		int i = 0;
		for (auto value : filter) {
			if (value.type == TypeEnum::CONCEPT) {
				auto &concept = value.get_concept_model()->members;
				for (int j = members.size() - 1; j >= 0; --j) {
					if (!concept[members[j][i]]) {
						members.erase(members.begin() + j);
					}
				}
				++i;
			} else {
				auto ind = value.get_individual()->index;
				for (int j = members.size() - 1; j >= 0; --j) {
					if (members[j][i] == ind) {
						members[j].erase(members[j].begin() + i);
					} else {
						members.erase(members.begin() + j);
					}
				}
			}
		}
		return members;
	}
};

class Set : public CollectionType {
public:
	Set() : CollectionType(TokenType::LEFT_BRACE, TokenType::RIGHT_BRACE) {}

	virtual concepts::Type *get_type(concepts::Type &input) {
		if (input == TypeEnum::INDIVIDUAL) {
			return new concepts::Type{ TypeEnum::CONCEPT };
		}

		if (input == TypeEnum::NUMBER) {
			return new concepts::Type{ TypeEnum::NUMBER_LIST };
		}

		type_error(TypeEnum::INDIVIDUAL, input);
		return nullptr;
	}

	virtual TypedValue *get_value(std::vector<TypedValue> &inputs, const Interpretation &interpretation) {
		if (inputs[0].type == TypeEnum::INDIVIDUAL) {
			auto model = std::make_shared<ConceptModel>(interpretation.universe_size, false);

			for (auto input : inputs) {
				model->members[input.get_individual()->index] = true;
			}

			return new TypedValue(model);
		}
		
		auto numbers = std::make_shared<NumberList>(inputs.size());

		for (unsigned int i = 0; i < inputs.size(); ++i) {
			numbers->number_list[i] = inputs[i].get_number()->number;
		}

		return new TypedValue(numbers);
	}
};

class Top : public LiteralType {
public:
	Top() : LiteralType(TokenType::TOP, TypeEnum::CONCEPT) {}

	virtual TypedValue *value(const Interpretation &interpretation, const Token &) {
		return new TypedValue(std::make_shared<ConceptModel>(interpretation.universe_size, true));
	}
};

class Bot : public LiteralType {
public:
	Bot() : LiteralType(concepts::TokenType::TOP, TypeEnum::CONCEPT) {}

	virtual TypedValue *value(const Interpretation &interpretation, const Token &) {
		return new TypedValue(std::make_shared<ConceptModel>(interpretation.universe_size, false));
	}
};

class Integer : public LiteralType {
public:
	Integer() : LiteralType(concepts::TokenType::INTEGER, TypeEnum::NUMBER) {}

	virtual TypedValue *value(const Interpretation &, const Token &token) {
		return new TypedValue(std::make_shared<Number>(std::stoi(token.lexeme)));
	}
};

class Not : public concepts::UnaryOperator {
public:
	Not() : UnaryOperator(TokenType::NOT) {}

	virtual concepts::Type *get_type(Type &input) {
		if (input != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, input);
		}

		return new concepts::Type{ TypeEnum::CONCEPT };
	}

	virtual TypedValue *get_value(TypedValue &input) {
		auto in = input.get_concept_model()->members;
		in.flip();
		return new TypedValue(std::make_shared<ConceptModel>(in));
	}
};

class Or : public BinaryOperator {
public:
	Or() : BinaryOperator(TokenType::OR) {}

	virtual concepts::Type *get_type(Type &left_input, Type &right_input) {
		if (left_input == TypeEnum::CONCEPT && right_input == TypeEnum::CONCEPT) {
			return new concepts::Type{ TypeEnum::CONCEPT };
		}

		if (left_input == TypeEnum::ROLE && right_input == TypeEnum::ROLE) {
			return new concepts::Type{ TypeEnum::ROLE };
		}

		type_error(TypeEnum::CONCEPT, left_input);

		return nullptr;
	}

	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) {
		if (left_input.type == TypeEnum::CONCEPT) {
			auto left = left_input.get_concept_model()->members;
			auto right = right_input.get_concept_model()->members;

			auto model = std::make_shared<ConceptModel>(left.size(), false);

			for (unsigned int i = 0; i < left.size(); ++i) {
				model->members[i] = left[i] || right[i];
			}

			return new TypedValue(model);
		} else {
			auto left = left_input.get_role_model()->members;
			auto right = right_input.get_role_model()->members;

			auto model = std::make_shared<RoleModel>(left.size());

			for (unsigned int i = 0; i < left.size(); ++i) {
				for (int ind : left[i]) {
					model->members[i].insert(ind);
				}
				for (int ind : right[i]) {
					model->members[i].insert(ind);
				}
			}

			return new TypedValue(model);
		}
	}
};

class And : public BinaryOperator {
public:
	And() : BinaryOperator(TokenType::AND) {}

	virtual concepts::Type *get_type(Type &left_input, Type &right_input) {
		if (left_input == TypeEnum::CONCEPT && right_input == TypeEnum::CONCEPT) {
			return new concepts::Type{ TypeEnum::CONCEPT };
		}

		if (left_input == TypeEnum::ROLE && right_input == TypeEnum::ROLE) {
			return new concepts::Type{ TypeEnum::ROLE };
		}

		type_error(TypeEnum::CONCEPT, left_input);

		return nullptr;
	}

	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) {
		if (left_input.type == TypeEnum::CONCEPT) {
			auto left = left_input.get_concept_model()->members;
			auto right = right_input.get_concept_model()->members;

			auto model = std::make_shared<ConceptModel>(left.size(), false);

			for (unsigned int i = 0; i < left.size(); ++i) {
				model->members[i] = left[i] && right[i];
			}

			return new TypedValue(model);
		} else {
			auto left = left_input.get_role_model()->members;
			auto right = right_input.get_role_model()->members;

			auto model = std::make_shared<RoleModel>(left.size());

			for (unsigned int i = 0; i < left.size(); ++i) {
				for (int ind : left[i]) {
					if (std::count(right[i].begin(), right[i].end(), ind) > 0) {
						model->members[i].insert(ind);
					}
				}
			}

			return new TypedValue(model);
		}
	}
};

class Exists : public BinaryOperator {
public:
	Exists() : BinaryOperator(TokenType::SOME) {}

	virtual concepts::Type *get_type(Type &left_input, Type &right_input) {
		if (left_input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, left_input);
		}

		if (right_input != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, right_input);
		}

		return new concepts::Type{ TypeEnum::CONCEPT };
	}

	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) {
		auto role = left_input.get_role_model()->members;
		auto concept = right_input.get_concept_model()->members;

		auto model = std::make_shared<ConceptModel>(concept.size(), false);

		for (unsigned int i = 0; i < role.size(); ++i) {
			for (unsigned int ind : role[i]) {
				if (concept[ind]) {
					model->members[i] = true;
					break;
				}
			}
		}

		return new TypedValue(model);
	}
};

class All : public BinaryOperator {
public:
	All() : BinaryOperator(TokenType::ALL) {}

	virtual concepts::Type *get_type(Type &left_input, Type &right_input) {
		if (left_input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, left_input);
		}

		if (right_input != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, right_input);
		}

		return new concepts::Type{ TypeEnum::CONCEPT };
	}

	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) {
		auto role = left_input.get_role_model()->members;
		auto concept = right_input.get_concept_model()->members;

		auto model = std::make_shared<ConceptModel>(concept.size(), true);

		for (unsigned int i = 0; i < role.size(); ++i) {
			for (unsigned int ind : role[i]) {
				if (!concept[ind]) {
					model->members[i] = false;
					break;
				}
			}
		}

		return new TypedValue(model);
	}
};

class BindingSome : public BindingType {
public:
	BindingSome() : BindingType(TokenType::SOME) {}

	virtual Type* get_type(Type &input, Type& right_input) {
		if (input != TypeEnum::CONCEPT && input != TypeEnum::ROLE && input != TypeEnum::PREDICATE) {
			type_error(TypeEnum::CONCEPT, input);
		}

		if (right_input != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, input);
		}

		return new Type{ input };
	}

	virtual TypedValue* get_value(std::function<TypedValue* (TypedValue&)> value_for_assignment, TypedValue &right_input, int universe_size) {
		TypedValue assignment;
		assignment = TypedValue(std::make_shared<Individual>(0));
		auto value = value_for_assignment(assignment);
		auto concept = right_input.get_concept_model()->members;
		if (value->type == TypeEnum::CONCEPT) {
			auto members = value->get_concept_model()->members;
			for (int i = 1; i < universe_size; ++i) {
				if (!concept[i]) {
					continue;
				}
				assignment = TypedValue(std::make_shared<Individual>(i));
				value = value_for_assignment(assignment);
				auto current_members = value->get_concept_model()->members;
				delete value;
				for (int ind = 0; ind < universe_size; ++ind) {
					members[ind] = members[ind] || current_members[ind];
				}
			}
			return new TypedValue(std::make_shared<ConceptModel>(members));
		} else if (value->type == TypeEnum::ROLE) {
			auto members = value->get_role_model()->members;
			for (int i = 1; i < universe_size; ++i) {
				if (!concept[i]) {
					continue;
				}
				assignment = TypedValue(std::make_shared<Individual>(i));
				value = value_for_assignment(assignment);
				auto current_members = value->get_role_model()->members;
				delete value;
				for (int ind = 0; ind < universe_size; ++ind) {
					for (auto mem : current_members[ind]) {
						members[ind].insert(mem);
					}
				}
			}
			return new TypedValue(std::make_shared<RoleModel>(members));
		} else {
			int arity = value->type.get_number();
			std::set<std::vector<int>> members;
			std::vector<std::vector<int>> m;
			switch (arity) {
			case 3:
				m = value->get_predicate_model<3>()->members;
				break;
			case 4:
				m = value->get_predicate_model<4>()->members;
				break;
			case 5:
				m = value->get_predicate_model<5>()->members;
				break;
			}
			for (auto mem : m) {
				members.insert(mem);
			}
			for (int i = 1; i < universe_size; ++i) {
				if (!concept[i]) {
					continue;
				}
				assignment = TypedValue(std::make_shared<Individual>(i));
				value = value_for_assignment(assignment);
				std::vector<std::vector<int>> current_members;
				switch (arity) {
				case 3:
					current_members = value->get_predicate_model<3>()->members;
					break;
				case 4:
					current_members = value->get_predicate_model<4>()->members;
					break;
				case 5:
					current_members = value->get_predicate_model<5>()->members;
					break;
				}
				delete value;
				for (auto member : current_members) {
					members.insert(member);
				}
			}
			switch (arity) {
			case 3:
				return new TypedValue(std::make_shared<PredicateModel<3>>(std::vector<std::vector<int>>(members.begin(), members.end())));
			case 4:
				return new TypedValue(std::make_shared<PredicateModel<4>>(std::vector<std::vector<int>>(members.begin(), members.end())));
			case 5:
				return new TypedValue(std::make_shared<PredicateModel<5>>(std::vector<std::vector<int>>(members.begin(), members.end())));
			}
			return nullptr;
		}
	}
};

class BindingAll : public BindingType {
public:
	BindingAll() : BindingType(TokenType::ALL) {}

	virtual Type* get_type(Type& input, Type& right_input) {
		if (input != TypeEnum::CONCEPT && input != TypeEnum::ROLE && input != TypeEnum::PREDICATE) {
			type_error(TypeEnum::CONCEPT, input);
		}

		if (right_input != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, input);
		}

		return new Type{ input };
	}

	virtual TypedValue* get_value(std::function<TypedValue* (TypedValue&)> value_for_assignment, TypedValue &right_input, int universe_size) {
		TypedValue assignment;
		assignment = TypedValue(std::make_shared<Individual>(0));
		auto value = value_for_assignment(assignment);
		auto concept = right_input.get_concept_model()->members;
		if (value->type == TypeEnum::CONCEPT) {
			auto members = value->get_concept_model()->members;
			for (int i = 1; i < universe_size; ++i) {
				if (concept[i]) {
					continue;
				}
				assignment = TypedValue(std::make_shared<Individual>(i));
				value = value_for_assignment(assignment);
				auto current_members = value->get_concept_model()->members;
				delete value;
				for (int ind = 0; ind < universe_size; ++ind) {
					members[ind] = members[ind] && current_members[ind];
				}
			}
			return new TypedValue(std::make_shared<ConceptModel>(members));
		} else if (value->type == TypeEnum::ROLE) {
			auto members = value->get_role_model()->members;
			for (int i = 1; i < universe_size; ++i) {
				if (concept[i]) {
					continue;
				}
				assignment = TypedValue(std::make_shared<Individual>(i));
				value = value_for_assignment(assignment);
				auto current_members = value->get_role_model()->members;
				delete value;
				for (int ind = 0; ind < universe_size; ++ind) {
					for (int non_mem = 0; non_mem < universe_size; ++non_mem) {
						if (current_members[ind].find(non_mem) == current_members[ind].end()) {
							members[ind].erase(non_mem);
						}
					}
				}
			}
			return new TypedValue(std::make_shared<RoleModel>(members));
		} else {
			int arity = value->type.get_number();
			std::set<std::vector<int>> members;
			std::vector<std::vector<int>> m;
			switch (arity) {
			case 3:
				m = value->get_predicate_model<3>()->members;
				break;
			case 4:
				m = value->get_predicate_model<4>()->members;
				break;
			case 5:
				m = value->get_predicate_model<5>()->members;
				break;
			}
			delete value;
			for (auto mem : m) {
				members.insert(mem);
			}
			for (int i = 1; i < universe_size; ++i) {
				if (concept[i]) {
					continue;
				}
				assignment = TypedValue(std::make_shared<Individual>(i));
				value = value_for_assignment(assignment);
				std::vector<std::vector<int>> current_members;
				switch (arity) {
				case 3:
					current_members = value->get_predicate_model<3>()->members;
					break;
				case 4:
					current_members = value->get_predicate_model<4>()->members;
					break;
				case 5:
					current_members = value->get_predicate_model<5>()->members;
					break;
				}
				// this is not right but likely not needed for now, use set_intersection?
				for (auto member : current_members) {
					members.erase(member);
				}
			}
			switch (arity) {
			case 3:
				return new TypedValue(std::make_shared<PredicateModel<3>>(std::vector<std::vector<int>>(members.begin(), members.end())));
			case 4:
				return new TypedValue(std::make_shared<PredicateModel<4>>(std::vector<std::vector<int>>(members.begin(), members.end())));
			case 5:
				return new TypedValue(std::make_shared<PredicateModel<5>>(std::vector<std::vector<int>>(members.begin(), members.end())));
			}
			return nullptr;
		}
	}
};

struct CheckStatus {
	bool is_true;
	bool can_change;

	CheckStatus(bool is_true, bool can_change) : is_true(is_true), can_change(can_change) {}
};

class Cardinality : public TernaryOperator {
protected:
	virtual CheckStatus check_count(int count, int bound) = 0;

public:
	Cardinality(TokenType left) : TernaryOperator(left, TokenType::ARE) {}

	virtual concepts::Type* get_type(concepts::Type& left_input, concepts::Type& middle_input, concepts::Type& right_input) {
		if (left_input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, left_input);
		}

		if (middle_input != TypeEnum::NUMBER) {
			type_error(TypeEnum::NUMBER, middle_input);
		}

		if (right_input != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, right_input);
		}

		return new concepts::Type{ TypeEnum::CONCEPT };
	}

	virtual TypedValue* get_value(TypedValue& left_input, TypedValue& middle_input, TypedValue& right_input) {
		auto role = left_input.get_role_model()->members;
		auto number = middle_input.get_number()->number;
		auto concept = right_input.get_concept_model()->members;

		auto model = std::make_shared<ConceptModel>(concept.size(), false);

		for (unsigned int i = 0; i < role.size(); ++i) {
			int count = 0;
			auto check = check_count(count, number);
			for (unsigned int ind : role[i]) {
				if (concept[ind]) {
					count++;
					check = check_count(count, number);
					if (!check.can_change) {
						break;
					}
				}
			}

			if (check.is_true) {
				model->members[i] = true;
			}
		}

		return new TypedValue(model);
	}
};

class Exactly : public Cardinality {
protected:
	virtual CheckStatus check_count(int count, int bound) {
		return CheckStatus(count == bound, count <= bound);
	}
public:
	Exactly() : Cardinality(TokenType::EXACTLY) {}
};

class AtLeast : public Cardinality {
protected:
	virtual CheckStatus check_count(int count, int bound) {
		return CheckStatus(count >= bound, count < bound);
	}
public:
	AtLeast() : Cardinality(TokenType::AT_LEAST) {}
};

class AtMost : public Cardinality {
protected:
	virtual CheckStatus check_count(int count, int bound) {
		return CheckStatus(count <= bound, count <= bound);
	}
public:
	AtMost() : Cardinality(TokenType::AT_MOST) {}
};

class LessThan : public Cardinality {
protected:
	virtual CheckStatus check_count(int count, int bound) {
		return CheckStatus(count < bound, count < bound);
	}
public:
	LessThan() : Cardinality(TokenType::LESS_THAN) {}
};

class MoreThan : public Cardinality {
protected:
	virtual CheckStatus check_count(int count, int bound) {
		return CheckStatus(count > bound, count <= bound);
	}
public:
	MoreThan() : Cardinality(TokenType::MORE_THAN) {}
};

class Equals : public BinaryOperator {
public:
	Equals() : BinaryOperator(TokenType::EQUAL_EQUAL) {}

	virtual concepts::Type *get_type(Type &left_input, Type &right_input) {
		if (left_input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, left_input);
		}

		if (right_input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, right_input);
		}

		return new concepts::Type{ TypeEnum::CONCEPT };
	}

	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) {
		auto left = left_input.get_role_model()->members;
		auto right = right_input.get_role_model()->members;

		auto model = std::make_shared<ConceptModel>(left.size(), false);

		for (unsigned int i = 0; i < left.size(); ++i) {
			if (left[i].size() != right[i].size()) {
				continue;
			}

			bool set = true;
			for (auto ind : left[i]) {
				if (right[i].find(ind) == right[i].end()) {
					set = false;
					break;
				}
			}
			if (set) model->members[i] = true;
		}

		return new TypedValue(model);
	}
};

class Compose : public BinaryOperator {
public:
	Compose() : BinaryOperator(TokenType::COMPOSE) {}

	virtual concepts::Type *get_type(Type &left_input, Type &right_input) {
		if (left_input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, left_input);
		}

		if (right_input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, right_input);
		}

		return new concepts::Type{ TypeEnum::ROLE };
	}

	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) {
		auto left = left_input.get_role_model()->members;
		auto right = right_input.get_role_model()->members;

		auto model = std::make_shared<RoleModel>(left.size());

		for (unsigned int i = 0; i < left.size(); ++i) {
			for (auto j : left[i]) {
				for (auto ind : right[j]) {
					model->members[i].insert(ind);
				}
			}
		}

		return new TypedValue(model);
	}
};

class Inverse : public concepts::UnaryOperator {
public:
	Inverse() : UnaryOperator(TokenType::MINUS) {}

	virtual concepts::Type *get_type(Type &input) {
		if (input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, input);
		}

		return new concepts::Type{ TypeEnum::ROLE };
	}

	virtual TypedValue *get_value(TypedValue &input) {
		auto in = input.get_role_model()->members;

		auto model = std::make_shared<RoleModel>(in.size());

		for (unsigned int i = 0; i < in.size(); ++i) {
			for (auto j : in[i]) {
				model->members[j].insert(i);
			}
		}

		return new TypedValue(model);
	}
};

class Closure : public concepts::UnaryOperator {
public:
	Closure() : UnaryOperator(TokenType::PLUS) {}

	virtual concepts::Type *get_type(Type &input) {
		if (input != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, input);
		}

		return new concepts::Type{ TypeEnum::ROLE };
	}

	virtual TypedValue *get_value(TypedValue &input) {
		auto in = input.get_role_model()->members;

		auto model = std::make_shared<RoleModel>(in.size());

		for (unsigned int i = 0; i < in.size(); ++i) {
			for (auto j : in[i]) {
				model->members[i].insert(j);
			}
		}

		bool changed;
		do {
			changed = false;
			for (unsigned int i = 0; i < model->members.size(); ++i) {
				for (auto j : model->members[i]) {
					for (auto ind : model->members[j]) {
						changed = changed || model->members[i].insert(ind).second;
					}
				}
			}
		} while (changed);

		return new TypedValue(model);
	}
};

class Product : public BinaryOperator {
public:
	Product() : BinaryOperator(TokenType::STAR) {}

	virtual concepts::Type *get_type(Type &left_input, Type &right_input) {
		if (left_input != TypeEnum::NUMBER) {
			type_error(TypeEnum::NUMBER, left_input);
		}

		if (right_input != TypeEnum::NUMBER) {
			type_error(TypeEnum::NUMBER, right_input);
		}

		return new concepts::Type{ TypeEnum::NUMBER };
	}

	virtual TypedValue *get_value(TypedValue &left_input, TypedValue &right_input) {
		int res = left_input.get_number()->number* right_input.get_number()->number;
		return new TypedValue(std::make_shared<Number>(res));
	}
};

class Dist : public Function {
public:
	Dist() : Function(concepts::TokenType::DIST, 3) {}

	virtual concepts::Type* get_type(std::vector<concepts::Type>& inputs) {
		if (inputs[0] != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, inputs[0]);
		}

		if (inputs[1] != TypeEnum::ROLE) {
			type_error(TypeEnum::ROLE, inputs[1]);
		}

		if (inputs[2] != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, inputs[2]);
		}

		return new concepts::Type{ TypeEnum::NUMBER };
	}

	virtual TypedValue* get_value(std::vector<TypedValue>& inputs) {
		auto visited = inputs[0].get_concept_model()->members;
		auto succ = inputs[1].get_role_model()->members;
		auto goal = inputs[2].get_concept_model()->members;

		std::list<int> initial;
		for (unsigned int i = 0; i < visited.size(); ++i) {
			if (visited[i]) {
				if (goal[i]) {
					return new TypedValue(std::make_shared<Number>(0));
				}
				initial.push_back(i);
			}
		}
		std::list<int> other;

		auto frontier = &initial;
		auto next = &other;

		int steps = 1;
		while (!frontier->empty()) {
			do  {
				int node = frontier->front();
				frontier->pop_front();
				for (unsigned int ind : succ[node]) {
					if (!visited[ind]) {
						if (goal[ind]) {
							return new TypedValue(std::make_shared<Number>(steps));
						}
						visited[ind] = true;
						next->push_back(ind);
					}
				}
			} while (!frontier->empty());

			auto temp = frontier;
			frontier = next;
			next = temp;
			++steps;
		}

		return new TypedValue(std::make_shared<Number>(0));
	}
};

class Card : public concepts::UnaryOperator {
public:
	Card() : UnaryOperator(TokenType::HASH) {}

	virtual concepts::Type* get_type(Type& input) {
		if (input != TypeEnum::CONCEPT) {
			type_error(TypeEnum::CONCEPT, input);
		}

		return new Type{ TypeEnum::NUMBER };
	}

	virtual TypedValue* get_value(TypedValue& input) {
		auto in = input.get_concept_model()->members;
		auto model = std::make_shared<Number>(std::count(in.begin(), in.end(), true));
		return new TypedValue(model);
	}
};

}

#endif
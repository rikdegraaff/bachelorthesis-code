#include <iostream>

#include "parser.h"

using namespace std;

namespace concepts {

void* TernaryExpr::accept(ExprVisitor& visitor) {
	return visitor.visit_aux(*this);
}

void* BinaryExpr::accept(ExprVisitor &visitor) {
	return visitor.visit_aux(*this);
}

void* UnaryExpr::accept(ExprVisitor &visitor) {
	return visitor.visit_aux(*this);
}

void* FunctionExpr::accept(ExprVisitor &visitor) {
	return visitor.visit_aux(*this);
}

void* LiteralExpr::accept(ExprVisitor &visitor) {
	return visitor.visit_aux(*this);
}

void* IdentifierExpr::accept(ExprVisitor &visitor) {
	return visitor.visit_aux(*this);
}

void* SubscriptExpr::accept(ExprVisitor& visitor) {
	return visitor.visit_aux(*this);
}

void* CollectionExpr::accept(ExprVisitor& visitor) {
	return visitor.visit_aux(*this);
}

void* BindingExpr::accept(ExprVisitor& visitor) {
	return visitor.visit_aux(*this);
}

void ParseError::print() const
{
	cerr << "concept parsing error: " << msg << endl;
	/*	<< "  at: line " << line ", col " << col << endl;
	if (!substring.empty()) {
		cerr << "  context: " << substring << endl;
	}*/
}

}
#ifndef CONCEPTS_PARSER_H
#define CONCEPTS_PARSER_H

#include <algorithm>
#include <functional>
#include <list>
#include <memory>
#include <numeric>
#include <string>
#include <unordered_map>
#include <vector>
#include <set>

#include "scanner.h"
#include "type_system.h"

namespace concepts {

template <typename T>
std::set<T> operator+(const std::set<T> &a, const std::set<T> &b) {
	std::set<T> result = a;
	result.insert(b.begin(), b.end());
	return result;
}

template <typename T>
std::set<T> operator-(const std::set<T> &a, T &b) {
	std::set<T> result = a;
	result.erase(b);
	return result;
}

class ParseError : public utils::Exception {
	const std::string msg;
public:
	ParseError(const std::string& error)
		: msg(error) {}

	virtual void print() const override;
};

class ExprVisitor;

class Expr {
public:
	const bool is_static;
	const std::set<std::string> free;
	bool has_alias;
	std::string alias;

	Expr(bool is_static, std::set<std::string> free) : is_static(is_static), free(free), has_alias(false), alias("") {}

	virtual ~Expr() {}
	virtual void *accept(ExprVisitor &) {
		return nullptr;
	}

	void set_alias(std::string alias) {
		has_alias = true;
		this->alias = alias;
	}
};

template <typename Exprs>
std::set<std::string> union_free(Exprs exprs) {
	std::set<std::string> free;
	for (std::shared_ptr<Expr> e : exprs) {
		free.insert(e->free.begin(), e->free.end());
	}
	return free;
}

class TernaryExpr : public Expr {
public:
	const std::shared_ptr<Expr> left;
	const std::shared_ptr<TernaryOperator> op;
	const std::shared_ptr<Expr> middle;
	const std::shared_ptr<Expr> right;

	TernaryExpr(std::shared_ptr<Expr> left, std::shared_ptr<TernaryOperator> op, std::shared_ptr<Expr> middle, std::shared_ptr<Expr> right) 
		: Expr(left->is_static && middle->is_static && right->is_static, left->free + middle->free + right->free), 
		left(left), op(op), middle(middle), right(right) {}

	virtual void *accept(ExprVisitor &visitor);

	bool operator==(const TernaryExpr &other) {
		return left == other.left && op == other.op && middle == other.middle && right == other.right;
	}
};

class BinaryExpr : public Expr {
public:
	const std::shared_ptr<Expr> left;
	const std::shared_ptr<BinaryOperator> op;
	const std::shared_ptr<Expr> right;

	BinaryExpr(std::shared_ptr<Expr> left, std::shared_ptr<BinaryOperator> op, std::shared_ptr<Expr> right) 
		: Expr(left->is_static && right->is_static, left->free + right->free), 
		left(left), op(op), right(right) {}

	virtual void *accept(ExprVisitor &visitor);

	bool operator==(const BinaryExpr &other) {
		return left == other.left && op == other.op && right == other.right;
	}
};

class UnaryExpr : public Expr {
public:
	const std::shared_ptr<Expr>expr;
	const std::shared_ptr<UnaryOperator> op;

	UnaryExpr(std::shared_ptr<Expr> expr, std::shared_ptr<UnaryOperator> op) 
		: Expr(expr->is_static, expr->free), expr(expr), op(op) {}

	virtual void *accept(ExprVisitor &visitor);

	bool operator==(const UnaryExpr &other) {
		return expr == other.expr && op == other.op;
	}
};

class FunctionExpr : public Expr {
public:
	const std::vector<std::shared_ptr<Expr>> args;
	const std::shared_ptr<Function> fun;

	FunctionExpr(std::vector<std::shared_ptr<Expr>> args, std::shared_ptr<Function> fun) 
		: Expr(std::all_of(args.begin(), args.end(), [](std::shared_ptr<Expr> &e) { return e->is_static; }),
			union_free(args)),
		args(args), fun(fun) {}

	virtual void* accept(ExprVisitor &visitor);

	bool operator==(const FunctionExpr &other) {
		return args == other.args && fun == other.fun;
	}
};

class LiteralExpr : public Expr {
public:
	const std::shared_ptr<LiteralType> type;
	const Token token;

	LiteralExpr(std::shared_ptr<LiteralType> type, Token token) : Expr(true, std::set<std::string>()), type(type), token(token) {}

	virtual void* accept(ExprVisitor& visitor);
};

class IdentifierExpr : public Expr {
public:
	const std::string name;
	const bool bindable;

	IdentifierExpr(std::string name, bool is_static, bool bindable) 
		: Expr(is_static, bindable ? std::set<std::string>({ name }) : std::set<std::string>()),
		name(name), bindable(bindable) {}

	virtual void* accept(ExprVisitor &visitor);

	bool operator==(const IdentifierExpr &other) {
		return name == other.name;
	}
};

class SubscriptExpr : public Expr {
public:
	const std::shared_ptr<Expr> base;
	const std::shared_ptr<Subscript> subscript;
	const std::vector<std::shared_ptr<Expr>> args;

	SubscriptExpr(std::shared_ptr<Expr> base, std::shared_ptr<Subscript> subscript, std::vector<std::shared_ptr<Expr>> args)
		: Expr(base->is_static && std::all_of(args.begin(), args.end(), [](std::shared_ptr<Expr>& e) { return e->is_static; }),
			base->free + union_free(args)),
		base(base), subscript(subscript), args(args) {}

	virtual void *accept(ExprVisitor &visitor);

	bool operator==(const SubscriptExpr &other) {
		return base == other.base && subscript == other.subscript && args == other.args;
	}
};

class CollectionExpr : public Expr {
public:
	const std::shared_ptr<CollectionType> type;
	const std::list<std::shared_ptr<Expr>> exprs;

	CollectionExpr(std::shared_ptr<CollectionType> type, std::list<std::shared_ptr<Expr>> exprs) 
		: Expr(std::all_of(exprs.begin(), exprs.end(), [](std::shared_ptr<Expr>& e) { return e->is_static; }),
			union_free(exprs)),
		type(type), exprs(exprs) {}

	virtual void* accept(ExprVisitor &visitor);

	bool operator==(const CollectionExpr &other) {
		return type == other.type && exprs == other.exprs;
	}
};

class BindingExpr : public Expr {
public:
	const std::shared_ptr<BindingType> type;
	const std::string var;
	const std::shared_ptr<Expr> expr;
	const std::shared_ptr<Expr> right_expr;

	BindingExpr(std::shared_ptr<BindingType> type, std::string var, std::shared_ptr<Expr> expr, std::shared_ptr<Expr> right_expr)
		: Expr(expr->is_static && right_expr->is_static, (expr->free - var) + right_expr->free),
		type(type), var(var), expr(expr), right_expr(right_expr) {}

	virtual void* accept(ExprVisitor &visitor);

	bool operator==(const BindingExpr &other) {
		return type == other.type && var == other.var && expr == other.expr && right_expr == other.right_expr;
	}
};

class ExprVisitor {
public:
	virtual ~ExprVisitor() {}

	virtual void *visit_aux(TernaryExpr &expr) = 0;
	virtual void *visit_aux(BinaryExpr &expr) = 0;
	virtual void *visit_aux(UnaryExpr &expr) = 0;
	virtual void *visit_aux(FunctionExpr& expr) = 0;
	virtual void *visit_aux(SubscriptExpr& expr) = 0;
	virtual void *visit_aux(IdentifierExpr &expr) = 0;
	virtual void *visit_aux(LiteralExpr &expr) = 0;
	virtual void *visit_aux(CollectionExpr &expr) = 0;
	virtual void *visit_aux(BindingExpr &expr) = 0;
};

class NoReturnExprVisitor : public ExprVisitor {
public:
	virtual void visit(TernaryExpr &expr) = 0;
	virtual void visit(BinaryExpr &expr) = 0;
	virtual void visit(UnaryExpr &expr) = 0;
	virtual void visit(FunctionExpr &expr) = 0;
	virtual void visit(SubscriptExpr& expr) = 0;
	virtual void visit(IdentifierExpr& expr) = 0;
	virtual void visit(LiteralExpr &expr) = 0;
	virtual void visit(CollectionExpr &expr) = 0;
	virtual void visit(BindingExpr &expr) = 0;

	void base_visit(std::shared_ptr<Expr> expr) {
		expr->accept(*this);
	}

	virtual void *visit_aux(TernaryExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void *visit_aux(BinaryExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void* visit_aux(UnaryExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void* visit_aux(FunctionExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void* visit_aux(SubscriptExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void* visit_aux(IdentifierExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void* visit_aux(LiteralExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void* visit_aux(CollectionExpr& expr) {
		visit(expr);
		return nullptr;
	}

	virtual void* visit_aux(BindingExpr& expr) {
		visit(expr);
		return nullptr;
	}
};

template<typename R>
class TypedExprVisitor : public ExprVisitor {
public:
	virtual R* visit(TernaryExpr& expr) = 0;
	virtual R* visit(BinaryExpr& expr) = 0;
	virtual R* visit(UnaryExpr& expr) = 0;
	virtual R* visit(FunctionExpr& expr) = 0;
	virtual R* visit(SubscriptExpr& expr) = 0;
	virtual R* visit(IdentifierExpr& expr) = 0;
	virtual R* visit(LiteralExpr& expr) = 0;
	virtual R* visit(CollectionExpr& expr) = 0;
	virtual R* visit(BindingExpr& expr) = 0;

	R* base_visit(std::shared_ptr<Expr> expr) {
		return (R*) expr->accept(*this);
	}

	virtual void* visit_aux(TernaryExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(BinaryExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(UnaryExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(FunctionExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(SubscriptExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(IdentifierExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(LiteralExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(CollectionExpr& expr) {
		return visit(expr);
	}

	virtual void* visit_aux(BindingExpr& expr) {
		return visit(expr);
	}
};

class ExprCache : public TypedExprVisitor<std::shared_ptr<Expr>> {
	std::set<std::shared_ptr<TernaryExpr>> ternary_exprs;
	std::set<std::shared_ptr<BinaryExpr>> binary_exprs;
	std::set<std::shared_ptr<UnaryExpr>> unary_exprs;
	std::set<std::shared_ptr<FunctionExpr>> function_exprs;
	std::set<std::shared_ptr<SubscriptExpr>> subscript_exprs;
	std::set<std::shared_ptr<IdentifierExpr>> identifier_exprs;
	std::set<std::shared_ptr<CollectionExpr>> collection_exprs;
	std::set<std::shared_ptr<BindingExpr>> binding_exprs;

	std::shared_ptr<Expr> current_expr;
public:
	std::shared_ptr<Expr> get_unique(std::shared_ptr<Expr> expr) {
		current_expr = expr;
		auto result_ptr = base_visit(expr);
		current_expr = nullptr;
		auto result = *result_ptr;
		delete result_ptr;
		return result;
	}

	virtual std::shared_ptr<Expr> *visit(TernaryExpr &expr) {
		for (auto e : ternary_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		ternary_exprs.insert(std::static_pointer_cast<TernaryExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr> *visit(BinaryExpr &expr) {
		for (auto e : binary_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		binary_exprs.insert(std::static_pointer_cast<BinaryExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr> *visit(UnaryExpr& expr) {
		for (auto e : unary_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		unary_exprs.insert(std::static_pointer_cast<UnaryExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr> *visit(FunctionExpr& expr) {
		for (auto e : function_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		function_exprs.insert(std::static_pointer_cast<FunctionExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr> *visit(SubscriptExpr& expr) {
		for (auto e : subscript_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		subscript_exprs.insert(std::static_pointer_cast<SubscriptExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr> *visit(IdentifierExpr& expr) {
		for (auto e : identifier_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		identifier_exprs.insert(std::static_pointer_cast<IdentifierExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr>* visit(LiteralExpr&) {
		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr> *visit(CollectionExpr& expr) {
		for (auto e : collection_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		collection_exprs.insert(std::static_pointer_cast<CollectionExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}

	virtual std::shared_ptr<Expr> *visit(BindingExpr& expr) {
		for (auto e : binding_exprs) {
			if (e->operator==(expr)) {
				return new std::shared_ptr<Expr>{ std::static_pointer_cast<Expr>(e) };
			}
		}

		binding_exprs.insert(std::static_pointer_cast<BindingExpr>(current_expr));

		return new std::shared_ptr<Expr>{ current_expr };
	}
};

class FixityLevel {
protected:
	template<typename Operators>
	static int match_operators(const Token &token, const Operators &operators) {
		for (unsigned int i = 0; i < operators.size(); ++i) {
			if (operators[i]->token == token.type) {
				return i;
			}
		}
		return -1;
	}
public:
	virtual ~FixityLevel() {}

	virtual std::shared_ptr<Expr> parse(std::list<Token>& tokens, std::function<std::shared_ptr<Expr>(std::list<Token>&)> parse_next_level, ExprCache &cache) = 0;
};

class UnaryFixityLevel: public FixityLevel {
	bool is_prefix;
	const std::vector<std::shared_ptr<concepts::UnaryOperator>> operators;
public:
	UnaryFixityLevel(bool is_prefix, std::vector<std::shared_ptr<concepts::UnaryOperator>> operators)
		: is_prefix(is_prefix), operators(operators) {}

	virtual std::shared_ptr<Expr> parse(std::list<Token>& tokens, std::function<std::shared_ptr<Expr>(std::list<Token>&)> parse_next_level, ExprCache &cache) {
		if (is_prefix) {
			int index = match_operators(tokens.front(), operators);
			if (index != -1) {
				tokens.pop_front();
				return cache.get_unique(std::make_shared<UnaryExpr>(parse(tokens, parse_next_level, cache), operators[index]));
			}
		}

		std::shared_ptr<Expr> expr = parse_next_level(tokens);

		if (!is_prefix) {
			int index = match_operators(tokens.front(), operators);
			while (index != -1) {
				tokens.pop_front();
				auto temp = expr;
				expr = cache.get_unique(std::make_shared<UnaryExpr>(temp, operators[index]));
				index = match_operators(tokens.front(), operators);
			}
		}

		return expr;
	}
};

class BinaryFixityLevel : public FixityLevel {
	const bool is_left_associative;
	const std::vector<std::shared_ptr<BinaryOperator>> operators;
public:
	BinaryFixityLevel(bool is_left_associative, std::vector<std::shared_ptr<BinaryOperator>> operators)
		: is_left_associative(is_left_associative), operators(operators) {}

	virtual std::shared_ptr<Expr> parse(std::list<Token> &tokens, std::function<std::shared_ptr<Expr>(std::list<Token>&)> parse_next_level, ExprCache &cache) {
		if (!is_left_associative) {
			std::shared_ptr<Expr> left = parse_next_level(tokens);
			int index = match_operators(tokens.front(), operators);
			if (index != -1) {
				tokens.pop_front();
				return cache.get_unique(std::make_shared<BinaryExpr>(left, operators[index], parse(tokens, parse_next_level, cache)));
			}

			return left;
		}
		
		std::shared_ptr<Expr> expr = parse_next_level(tokens);

		int index = match_operators(tokens.front(), operators);
		while (index != -1) {
			tokens.pop_front();
			auto temp = expr;
			expr = cache.get_unique(std::make_shared<BinaryExpr>(temp, operators[index], parse_next_level(tokens)));
			index = match_operators(tokens.front(), operators);
		}

		return expr;
	}
};

class TernaryFixityLevel : public FixityLevel {
	const bool is_left_associative;
	const std::vector<std::shared_ptr<TernaryOperator>> operators;
	const std::vector<std::shared_ptr<BindingType>> binding_operators;
public:
	TernaryFixityLevel(bool is_left_associative, std::vector<std::shared_ptr<TernaryOperator>> operators)
		: is_left_associative(is_left_associative), operators(operators) {}

	TernaryFixityLevel(bool is_left_associative, std::vector<std::shared_ptr<TernaryOperator>> operators, std::vector<std::shared_ptr<BindingType>> binding_operators)
		: is_left_associative(is_left_associative), operators(operators), binding_operators(binding_operators) {}


	virtual std::shared_ptr<Expr> parse(std::list<Token>& tokens, std::function<std::shared_ptr<Expr>(std::list<Token>&)> parse_next_level, ExprCache &cache) {
		if (tokens.front().type == TokenType::BINDABLE_VARIABLE) {
			auto var = tokens.front();
			tokens.pop_front();
			int index = match_operators(tokens.front(), binding_operators);
			if (index != -1) {
				tokens.pop_front();
				auto middle_expr = parse(tokens, parse_next_level, cache);
				if (tokens.front().type != binding_operators[index]->right_token) {
					throw ParseError("Expected " + token_string(operators[index]->right_token) + " to finish ternary operator at: line "
						+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
						+ " but encountered " + token_string(tokens.front().type) + " instead");
				}
				tokens.pop_front();
				auto right_expr = is_left_associative ? parse_next_level(tokens) : parse(tokens, parse_next_level, cache);
				return cache.get_unique(std::make_shared<BindingExpr>(binding_operators[index], var.lexeme, middle_expr, right_expr));
			}
			tokens.push_front(var);
		}

		std::shared_ptr<Expr> left = parse_next_level(tokens);

		int index = match_operators(tokens.front(), operators);
		while (index != -1) {
			tokens.pop_front();
			std::shared_ptr<Expr> middle = parse(tokens, parse_next_level, cache);

			if (tokens.front().type != operators[index]->right_token) {
				throw ParseError("Expected " + token_string(operators[index]->right_token) + " to finish ternary operator at: line "
					+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
					+ " but encountered " + token_string(tokens.front().type) + " instead");
			}
			tokens.pop_front();

			if (is_left_associative) {
				left = cache.get_unique(std::make_shared<TernaryExpr>(left, operators[index], middle, parse_next_level(tokens)));
				index = match_operators(tokens.front(), operators);
			} else {
				return cache.get_unique(std::make_shared<TernaryExpr>(left, operators[index], middle, parse(tokens, parse_next_level, cache)));
			}
		}

		return left;
	}
};

class ConceptParser {
	const std::vector<std::shared_ptr<FixityLevel>> levels;
	const std::vector<std::shared_ptr<CollectionType>> collection_types;
	const std::vector<std::shared_ptr<LiteralType>> literal_types;
	const std::vector<std::shared_ptr<Function>> functions;
	const std::shared_ptr<Subscript> subscript_type;
	std::unordered_map<std::string, std::shared_ptr<Expr>> aliases;
	std::set<std::string> identifiers;
	std::set<std::string> statics;

	ExprCache cache;
public:
	ConceptParser(
		std::vector<std::shared_ptr<FixityLevel>> &&levels, 
		std::vector<std::shared_ptr<CollectionType>> &&collection_types,
		std::vector<std::shared_ptr<LiteralType>> &&literal_types,
		std::vector<std::shared_ptr<Function>> &&functions,
		std::shared_ptr<Subscript> subscript_type,
		std::set<std::string> statics
	)
		: levels(move(levels)), collection_types(move(collection_types)), 
		literal_types(move(literal_types)), functions(move(functions)),
		subscript_type(subscript_type), statics(statics), cache()
	{}

	std::shared_ptr<Expr> parse(std::list<Token>& tokens) {
		while (tokens.front().type != TokenType::RETURN) {
			parse_assignment(tokens);
		}

		return parse_result(tokens);
	}

	std::set<std::string> get_identifiers() {
		return identifiers;
	}

private:
	void parse_assignment(std::list<Token>& tokens) {
		auto identifier = parse_indentifier(tokens);
		if (identifier == nullptr) {
			throw ParseError("Expected identifier or return at the beginning of statement at: line "
				+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
				+ " but encountered " + token_string(tokens.front().type) + " instead");
		}

		if (tokens.front().type != TokenType::EQUAL) {
			throw ParseError("Expected '=' for assignment to " + identifier->name + " at: line "
				+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
				+ " but encountered " + token_string(tokens.front().type) + " instead");
		}

		tokens.pop_front();

		auto expr = parse_expression(tokens);
		match_dot(tokens);

		expr->set_alias(identifier->name);

		aliases[identifier->name] = expr;
	}

	std::shared_ptr<Expr> parse_result(std::list<Token>& tokens) {
		tokens.pop_front();

		auto expr = parse_expression(tokens);

		match_dot(tokens);

		if (tokens.front().type != TokenType::END_OF_FILE) {
			throw ParseError("Expected end of file after return at: line "
				+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
				+ " but encountered " + token_string(tokens.front().type) + " instead");
		}
		tokens.pop_front();

		return expr;
	}

	std::shared_ptr<Expr> parse_expression(std::list<Token>& tokens) {
		return parse_expression(tokens, 0);
	}

	std::shared_ptr<Expr> parse_expression(std::list<Token>& tokens, const unsigned int level) {
		if (level == levels.size() - 1) {
			return levels.back()->parse(tokens, [this](std::list<Token> &ts) { return parse_subscript(ts); }, cache);
		}
		return levels[level]->parse(tokens, [this, level](std::list<Token> &ts){ return parse_expression(ts, level + 1); }, cache);
	}

	std::shared_ptr<Expr> parse_subscript(std::list<Token>& tokens) {
		auto expr = parse_primary(tokens);
		if (tokens.front().type != TokenType::LEFT_BRACKET || subscript_type == nullptr) {
			return expr;
		}

		Token left = tokens.front();
		tokens.pop_front();
		std::vector<std::shared_ptr<Expr>> exprs;
		while (tokens.front().type != TokenType::RIGHT_BRACKET) {
			exprs.push_back(parse_expression(tokens));

			if (tokens.front().type == TokenType::RIGHT_BRACKET) {
				break;
			}

			if (tokens.front().type != concepts::TokenType::COMMA) {
				throw ParseError("Subscript opened at: line " + std::to_string(left.line) + " col " + std::to_string(left.col)
					+ " not closed when  " + tokens.front().lexeme + " was encountered at: line "
					+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col));
			}

			tokens.pop_front();
		}

		tokens.pop_front();
		return cache.get_unique(std::make_shared<SubscriptExpr>(expr, subscript_type, exprs));
	}

	std::shared_ptr<Expr> parse_primary(std::list<Token> &tokens) {
		if (tokens.front().type == TokenType::END_OF_FILE) {
			throw ParseError("Unexpectedly encountered end of file");
		}

		if (tokens.front().type == TokenType::DOT) {
			throw ParseError("Unexpectedly encountered end of statement at: line "
				+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col));
		}

		for (auto literal_type : literal_types) {
			if (tokens.front().type == literal_type->token) {
				auto token = tokens.front();
				tokens.pop_front();
				return cache.get_unique(std::make_shared<LiteralExpr>(literal_type, token));
			}
		}

		for (auto collection_type : collection_types) {
			if (tokens.front().type == collection_type->left_delimitor) {
				Token left = tokens.front();
				tokens.pop_front();
				std::list<std::shared_ptr<Expr>> exprs;
				while (tokens.front().type != collection_type->right_delimitor) {
					exprs.push_back(parse_expression(tokens));
					
					if (tokens.front().type == collection_type->right_delimitor) {
						break;
					}

					if (tokens.front().type != concepts::TokenType::COMMA) {
						throw ParseError("Collection opened at: line " + std::to_string(left.line) + " col " + std::to_string(left.col)
							+ " not closed when  " + tokens.front().lexeme + " was encountered at: line "
							+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col));
					}

					tokens.pop_front();
				}

				tokens.pop_front();
				return cache.get_unique(std::make_shared<CollectionExpr>(collection_type, exprs));
			}
		}

		for (auto fun : functions) {
			if (tokens.front().type == fun->token) {
				auto f = tokens.front();
				tokens.pop_front();
				if (tokens.front().type != concepts::TokenType::LEFT_PAREN) {
					throw ParseError("Expected parenthesis after function " + token_string(f.type)
						+ " at: line " + std::to_string(f.line) + " col " + std::to_string(f.col)
						+ " but encountered " + token_string(tokens.front().type) + " instead");
				}

				std::vector<std::shared_ptr<Expr>> args(fun->arity);
				tokens.pop_front();
				for (int i = 0; i < fun->arity; ++i) {
					if (i != 0) {
						if (tokens.front().type != concepts::TokenType::COMMA) {
							throw ParseError("Expected comma after argument to function " + token_string(f.type)
								+ " with arity " + std::to_string(fun->arity) +
								+ " at: line " + std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
								+ " but encountered " + token_string(tokens.front().type) + " instead");
						}
						tokens.pop_front();
					}
					args[i] = parse_expression(tokens);
				}

				if (tokens.front().type != concepts::TokenType::RIGHT_PAREN) {
					throw ParseError("Expected parenthisis after " + std::to_string(fun->arity) + " arguments to function " + token_string(f.type)
						+" at: line " + std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
						+ " but encountered " + token_string(tokens.front().type) + " instead");
				}

				tokens.pop_front();

				return cache.get_unique(std::make_shared<FunctionExpr>(args, fun));
			}
		}

		auto expr = parse_indentifier(tokens);

		if (expr != nullptr) {
			auto search = aliases.find(expr->name);
			if (search == aliases.end()) {
				identifiers.insert(expr->name);
				return expr;
			} else {
				return search->second;
			}
		}

		if (tokens.front().type == TokenType::LEFT_PAREN) {
			Token left_paren = tokens.front();
			tokens.pop_front();
			std::shared_ptr<Expr> expr = parse_expression(tokens);
			if (tokens.front().type != TokenType::RIGHT_PAREN) {
				throw ParseError("Parenthesis opened at: line " + std::to_string(left_paren.line) + " col " + std::to_string(left_paren.col)
					+ " not closed when  " + tokens.front().lexeme + " was encountered at: line "
					+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col));
			}
			tokens.pop_front();
			return expr;
		}

		throw ParseError("Encountered unexpected token " + tokens.front().lexeme 
			+ "at: line " + std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col));
	}

	std::shared_ptr<IdentifierExpr> parse_indentifier(std::list<concepts::Token>& tokens) {
		if (tokens.front().type == TokenType::IDENTIFIER || tokens.front().type == TokenType::BINDABLE_VARIABLE) {
			Token token = tokens.front();
			bool bindable = tokens.front().type == TokenType::BINDABLE_VARIABLE;
			bool is_static = bindable || std::find(statics.begin(), statics.end(), token.lexeme) != statics.end();
			tokens.pop_front();
			return std::static_pointer_cast<IdentifierExpr>(cache.get_unique(std::make_shared<IdentifierExpr>(token.lexeme, is_static, bindable)));
		}

		return nullptr;
	}

	void match_dot(std::list<concepts::Token>& tokens) {
		if (tokens.front().type != TokenType::DOT) {
			throw ParseError("Expected period after end of statement at: line "
				+ std::to_string(tokens.front().line) + " col " + std::to_string(tokens.front().col)
				+ " but encountered " + token_string(tokens.front().type) + " instead");
		}
		tokens.pop_front();
	}
};

}

#endif
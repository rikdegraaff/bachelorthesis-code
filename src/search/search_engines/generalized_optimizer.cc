#include "generalized_optimizer.h"

#include "../option_parser.h"
#include "../plugin.h"
#include "../search_engine.h"
#include "../task_utils/task_properties.h"
#include "../evaluators/multi_evaluator.h"

#include "../utils/strings.h"

#include <fstream>
#include <iostream>
#include <queue>
#include <memory>
#include <set>

using namespace std;
using namespace utils;

namespace generalized_optimizer {

void GeneralizedOptimizer::write_to_file() {
	auto &states = tracker->get_states();
	auto &succ = tracker->get_successors();
	auto &hstar = tracker->get_hstar();

	ofstream state_space_output, evals_output;
	if (sample) {
		state_space_output.open(output_file + ".ss", ios::out | ios::trunc);
		write_interpretation(state_space_output);
		write_operators(state_space_output);
	}
	evals_output.open(output_file + ".pf", ios::out | ios::trunc);
	write_evaluator_names(evals_output);

	for (unsigned int i = 0; i < states.size(); ++i) {
		if ((i + 1) % 250 == 0) {
			cout << (i + 1) << "/" << states.size() << endl;
		}
		State state = task_proxy.convert_ancestor_state(states[i].unpack());
		if (sample) {
			int hs = hstar[i];
			auto successors = succ[i];

			bool first = true;
			for (int val : state.get_values()) {
				if (!first) {
					state_space_output << ",";
				}
				first = false;
				state_space_output << val;
			}

			state_space_output << ";" << hs << ";";

			first = true;
			for (auto s : successors) {
				if (!first) {
					state_space_output << ",";
				}
				first = false;
				state_space_output << s.state << "|" << s.op;
			}

			state_space_output << endl;
		}

		vector<int> evaluations;
		for (auto evaluator : evaluators) {
			evaluator->add_evaluations(state, evaluations);
			evaluator->add_evaluations(states[i], evaluations);
		}

		for (unsigned int j = 0; j < evaluations.size(); j++) {
			if (j > 0) {
				evals_output << ",";
			}
			evals_output << evaluations[j];
		}

		evals_output << endl;
	}

	if (sample) {
		state_space_output.close();
	}
	evals_output.close();
}

void GeneralizedOptimizer::write_evaluator_names(ofstream &evals_output) {
	vector<string> names;
	for (auto evaluator : evaluators) {
		evaluator->add_evaluator_names(names);
	}

	bool first = true;
	for (auto name : names) {
		if (!first) {
			evals_output << ";";
		}
		first = false;
		evals_output << name;
	}

	evals_output << endl;
}

void GeneralizedOptimizer::write_interpretation(ofstream &state_space_output) {
	vector<string> parts;
	set<string> individuals;
	vector<vector<string>> variables;
	for (auto var : task_proxy.get_variables()) {
		vector<string> values;
		for (int value = 0; value < var.get_domain_size(); ++value) {
			string name = var.get_fact(value).get_name();
			if (name[0] != 'A') {
				values.push_back("");
				continue;
			}
			parts.clear();
			replace_occurences(name, '(', ' ');
			replace_occurences(name, ')', ' ');
			name.erase(remove(name.begin(), name.end(), ','), name.end());
			replace_occurences(name, '-', '_');
			rstrip(name);
			auto pair = split(name, " ");
			if (pair.first != "Atom") {
				values.push_back("");
				continue;
			}
			parts.clear();
			values.push_back(pair.second);
			split_all(pair.second, parts);
			for (unsigned int i = 1; i < parts.size(); ++i) {
				individuals.insert(parts[i]);
			}
		}
		variables.push_back(values);
	}

	bool first = true;
	for (auto individual : individuals) {
		if (!first) {
			state_space_output << ",";
		}
		first = false;
		state_space_output << individual;
	}
	
	for (auto variable : variables) {
		state_space_output << ";";
		first = true;
		for (auto value : variable) {
			if (!first) {
				state_space_output << ",";
			}
			first = false;
			state_space_output << value;
		}
	}
	state_space_output << endl;
}

void GeneralizedOptimizer::write_operators(ofstream &state_space_output) {
	bool first = true;
	for (auto op : task_proxy.get_operators()) {
		if (!first) {
			state_space_output << ",";
		}
		first = false;
		state_space_output << op.get_name();
	}
	state_space_output << endl;
}

SearchStatus GeneralizedOptimizer::step() {
	auto status = inner_engine->step();
	switch (status) {
	case SearchStatus::FAILED:
		cout << "state space explored" << endl;
		tracker->backtrack();
		cout << "tracked back" << endl;
		write_to_file();
	default:
		return status;
	}
}

void GeneralizedOptimizer::initialize() {
	inner_engine->initialize();
	inner_engine->add_tracker(tracker);
	inner_engine->exhaustive = true;
}

GeneralizedOptimizer::GeneralizedOptimizer(const options::Options& opts)
	: SearchEngine(opts),
	//lp_solver(lp::LPSolverType(opts.get_enum("lpsolver"))),
	inner_engine(opts.get<shared_ptr<SearchEngine>>("engine")),
	evaluators(opts.get_list<shared_ptr<MultiEvaluator>>("evals")),
	tracker(new StateSpaceTracker(task_proxy)),
	output_file(opts.get<string>("output_file")),
	sample(opts.get<bool>("sample")) {}

GeneralizedOptimizer::~GeneralizedOptimizer() {
	delete tracker;
}

void GeneralizedOptimizer::print_statistics() const {
	inner_engine->print_statistics();
}

static shared_ptr<SearchEngine> _parse(OptionParser& parser) {
	parser.document_synopsis("Generalized potential heuristic optimizer", "");
	
	parser.add_list_option<shared_ptr<MultiEvaluator>>("evals", "The evaluators.");

	parser.add_option<string>("output_file");

	parser.add_option<bool>("sample", "set to false if the .ss file should not be created", "true");

	parser.add_option<shared_ptr<SearchEngine>>("engine", 
		"The engine to use for the initial search. This engine should report transitions.",
		"eager_greedy([blind()])");

	//lp::add_lp_solver_option_to_parser(parser);
	
	SearchEngine::add_pruning_option(parser);
	SearchEngine::add_options_to_parser(parser);
	Options opts = parser.parse();
	
	return parser.dry_run()
		? nullptr
		: make_shared<GeneralizedOptimizer>(opts);
}

static Plugin<SearchEngine> _plugin("sample", _parse);

void StateSpaceTracker::backtrack() {
	hstar.resize(successors.size(), -1);
	priority_queue<Successor, std::vector<Successor>, std::greater<Successor>> open;
	
	for (auto goal : goals) {
		open.emplace(0, 0, goal);
	}

	while (!open.empty()) {
		auto current = open.top();
		open.pop();
		if (hstar[current.state] == -1) {
			hstar[current.state] = current.cost;

			for (auto succ : predecessors[current.state]) {
				if (hstar[succ.state] == -1) {
					open.emplace(current.cost + succ.cost, succ.op, succ.state);
				}
			}
		}
	}
}

const vector<set<Successor>> &StateSpaceTracker::get_successors() {
	return successors;
}

const vector<GlobalState> &StateSpaceTracker::get_states() {
	return states;
}

const vector<int> &StateSpaceTracker::get_hstar() {
	return hstar;
}

void StateSpaceTracker::get_path_dependent_evaluators(std::set<Evaluator*>& evals) {
	evals.insert(this);
}

void StateSpaceTracker::notify_state_transition(const GlobalState& parent_state, OperatorID op_id, const GlobalState& state) {
	unsigned int parent_id = parent_state.get_id().value;
	unsigned int state_id = state.get_id().value;

	if (parent_id >= states.size()) {
		if (task_properties::is_goal_state(task_proxy, parent_state)) {
			mark_goal(states.size());
		}
		states.push_back(parent_state);
		predecessors.emplace_back();
		successors.emplace_back();
	}

	if (state_id >= states.size()) {
		if (task_properties::is_goal_state(task_proxy, state)) {
			mark_goal(states.size());
		}
		states.push_back(state);
		predecessors.emplace_back();
		successors.emplace_back();
	}

	if (state_id != parent_id) {
		int cost = task_proxy.get_operators()[op_id].get_cost();
		predecessors[state_id].emplace(cost, op_id.get_index(), parent_id);
		successors[parent_id].emplace(cost, op_id.get_index(), state_id);
	}
}

EvaluationResult StateSpaceTracker::compute_result(EvaluationContext&) {
	EvaluationResult res;
	res.set_evaluator_value(0);
	return res;
}

void StateSpaceTracker::mark_goal(int state) {
	goals.insert(state);
	cout << "found a goal" << endl;
}

}
#ifndef SEARCH_ENGINES_GENERALIZED_OPTIMIZER_H
#define SEARCH_ENGINES_GENERALIZED_OPTIMIZER_H

#include "../evaluator.h"
#include "../evaluation_result.h"
#include "../search_engine.h"
#include "../task_proxy.h"

#include "../lp/lp_solver.h"

#include <iostream>
#include <memory>
#include <set>
#include <vector>

namespace options {
	class OptionParser;
	class Options;
}

class MultiEvaluator;

namespace generalized_optimizer {

struct Successor {
	int cost;
	int op;
	int state;

	Successor(int cost, int op, int state) : cost(cost), op(op), state(state) {}

	bool operator<(const Successor &rhs) const {
		return cost < rhs.cost || (cost == rhs.cost && state < rhs.state);
	}

	bool operator>(const Successor &rhs) const {
		return cost > rhs.cost || (cost == rhs.cost && state > rhs.state);
	}
};

class StateSpaceTracker : public Evaluator {
	std::vector<GlobalState> states;
	std::vector<std::set<Successor>> predecessors;
	std::vector<std::set<Successor>> successors;
	std::vector<int> hstar;
	std::set<int> goals;
	TaskProxy task_proxy;
public:
	StateSpaceTracker(TaskProxy task_proxy) : task_proxy(task_proxy) {}

	virtual void get_path_dependent_evaluators(std::set<Evaluator*>& evals);

	virtual void notify_state_transition(
		const GlobalState& parent_state,
		OperatorID op_id,
		const GlobalState& state);

	virtual EvaluationResult compute_result(EvaluationContext& eval_context);

	void mark_goal(int state);

	void backtrack();

	const std::vector<std::set<Successor>> &get_successors();

	const std::vector<GlobalState> &get_states();

	const std::vector<int> &get_hstar();
};

class GeneralizedOptimizer : public SearchEngine {
	//lp::LPSolver lp_solver;
	std::shared_ptr<SearchEngine> inner_engine;
	std::vector<std::shared_ptr<MultiEvaluator>> evaluators;
	StateSpaceTracker *tracker;
	std::string output_file;
	bool sample;

	void write_to_file();
	void write_evaluator_names(std::ofstream &evals_output);
	void write_interpretation(std::ofstream &state_space_output);
	void write_operators(std::ofstream &state_space_output);
protected:
	virtual SearchStatus step();
	virtual void initialize();
public:
	GeneralizedOptimizer(const options::Options& opts);
	~GeneralizedOptimizer();
	virtual void print_statistics() const;
};

}

#endif
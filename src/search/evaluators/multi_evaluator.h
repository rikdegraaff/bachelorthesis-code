#ifndef EVALUATORS_MULTI_EVALUATOR_H
#define EVALUATORS_MULTI_EVALUATOR_H

#include "../task_proxy.h"

#include <memory>
#include <vector>
#include <string>

namespace options {
	class OptionParser;
	class Options;
}

class MultiEvaluator {
protected:
	const std::shared_ptr<AbstractTask> task;
	TaskProxy task_proxy;
public:
	static void add_options_to_parser(options::OptionParser &parser);

	MultiEvaluator(options::Options& opts);
	virtual ~MultiEvaluator() {}
	virtual void add_evaluations(const State&, std::vector<int>&) {}
	virtual void add_evaluations(const GlobalState&, std::vector<int>&) {}
	virtual void add_evaluator_names(std::vector<std::string>&) = 0;
};

#endif
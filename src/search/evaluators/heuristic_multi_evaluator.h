#ifndef HEURISTIC_EVALUATORS_MULTI_EVALUATOR_H
#define HEURISTIC_EVALUATORS_MULTI_EVALUATOR_H

#include "../task_proxy.h"
#include "../heuristic.h"

#include "multi_evaluator.h"

#include <memory>

namespace options {
	class Options;
}

class HeuristicMultiEvaluator : public MultiEvaluator {
	std::shared_ptr<Heuristic> heuristic;
public:
	HeuristicMultiEvaluator(options::Options &opts);
	virtual ~HeuristicMultiEvaluator() {}
	virtual void add_evaluations(const GlobalState& state, std::vector<int> &evaluations);
	virtual void add_evaluator_names(std::vector<std::string> &names);
};

#endif
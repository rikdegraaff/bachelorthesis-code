#include "heuristic_multi_evaluator.h"

#include "../option_parser.h"
#include "../plugin.h"

using namespace std;

HeuristicMultiEvaluator::HeuristicMultiEvaluator(options::Options &opts) 
	: MultiEvaluator(opts),
	heuristic(static_pointer_cast<Heuristic>(opts.get<shared_ptr<Evaluator>>("heuristic"))) {}

void HeuristicMultiEvaluator::add_evaluations(const GlobalState &state, std::vector<int> &evaluations) {
	evaluations.push_back(heuristic->compute_heuristic(state));
}

void HeuristicMultiEvaluator::add_evaluator_names(std::vector<std::string> &names) {
	names.push_back(heuristic->get_description());
}

static shared_ptr<MultiEvaluator> _parse(OptionParser &parser) {
	parser.document_synopsis("Multi evaluator wrapper for a single heuristic", "");

	parser.add_option<shared_ptr<Evaluator>>("heuristic");

	MultiEvaluator::add_options_to_parser(parser);
	Options opts = parser.parse();
	if (parser.dry_run())
		return nullptr;
	else
		return make_shared<HeuristicMultiEvaluator>(opts);
}

static Plugin<MultiEvaluator> _plugin("h", _parse);
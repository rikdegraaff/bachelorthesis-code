#include "multi_evaluator.h"

#include "../option_parser.h"
#include "../plugin.h"

#include <memory>

using namespace std;

void MultiEvaluator::add_options_to_parser(options::OptionParser &parser) {
    parser.add_option<shared_ptr<AbstractTask>>(
        "transform",
        "Optional task transformation for the heuristic."
        " Currently, adapt_costs() and no_transform() are available.",
        "no_transform()");
}

MultiEvaluator::MultiEvaluator(options::Options &opts)
	: task(opts.get<shared_ptr<AbstractTask>>("transform")),
	task_proxy(*task) {}

static PluginTypePlugin<MultiEvaluator> _type_plugin(
    "multi_evaluator",
    "");
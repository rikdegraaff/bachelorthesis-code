#include "weighted_sum_heuristic.h"

#include "../option_parser.h"
#include "../plugin.h"

#include <iostream>

using namespace std;

namespace weighted_sum_heuristic {

int WeightedSumHeuristic::compute_heuristic(const GlobalState& global_state) {
	State state = convert_global_state(global_state);
	return compute_heuristic(state, global_state);
}

int WeightedSumHeuristic::compute_heuristic(const State &state, const GlobalState &global_state) {
	vector<int> evaluations;
	evaluations.reserve(weights.size());

	for (auto evaluator : evaluators) {
		evaluator->add_evaluations(state, evaluations);
		evaluator->add_evaluations(global_state, evaluations);
	}

	float sum = 0;

	for (unsigned int i = 0; i < evaluations.size(); ++i) {
		if (evaluations[i] == DEAD_END) {
			return DEAD_END;
		}
		sum += weights[i] * evaluations[i];
	}

	return (int) round(sum);
}

WeightedSumHeuristic::WeightedSumHeuristic(const options::Options& opts)
	: Heuristic(opts),
	evaluators(opts.get_list<shared_ptr<MultiEvaluator>>("evals")),
	weights(opts.get_list<float>("weights")) {}

static shared_ptr<Evaluator> _parse(OptionParser& parser) {
	parser.document_synopsis("Weighted sum of evaluators heuristic", "");

	parser.add_list_option<shared_ptr<MultiEvaluator>>("evals", "The evaluators.");
	parser.add_list_option<float>("weights", "The weights.");

	Heuristic::add_options_to_parser(parser);
	Options opts = parser.parse();

	return parser.dry_run()
		? nullptr
		: make_shared<WeightedSumHeuristic>(opts);
}

static Plugin<Evaluator> _plugin("weighted_sum", _parse);

}

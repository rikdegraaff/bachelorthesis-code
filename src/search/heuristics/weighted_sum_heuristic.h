#ifndef HEURISTICS_WEIGHTED_SUM_HEURISTIC_H
#define HEURISTICS_WEIGHTED_SUM_HEURISTIC_H

#include "../heuristic.h"

#include "../evaluators/multi_evaluator.h"

#include <memory>
#include <vector>

namespace weighted_sum_heuristic {
    class WeightedSumHeuristic : public Heuristic {
        std::vector<std::shared_ptr<MultiEvaluator>> evaluators;
        std::vector<float> weights;
    protected:
        virtual int compute_heuristic(const GlobalState &global_state);
        int compute_heuristic(const State &state, const GlobalState &global_state);
    public:
        WeightedSumHeuristic(const options::Options& opts);
        ~WeightedSumHeuristic() {};
    };
}

#endif
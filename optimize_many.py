from general_optimizer import main as optimize
import sys
import os
from random import seed

def main():
    base_file_name = sys.argv[1]
    variants = int(sys.argv[2])
    real_args = sys.argv
    res = []
    f = open("heuristics.out", "w")
    f.write("")
    f.close()
    for i in range(variants):
        cpt = f"concepts({base_file_name}{i}.cpt)"
        sys.argv = [real_args[0], "--evaluators", cpt] + real_args[3:]
        seed(1729)
        ret = optimize()
        f = open("heuristics.out", "a")
        f.write(str(ret) + os.linesep)
        f.close()
        res.append(ret)
        sys.argv = [real_args[0], "--evaluators", "h(ff())", cpt] + real_args[3:]
        seed(1729)
        ret = optimize()
        f = open("heuristics.out", "a")
        f.write(str(ret) + os.linesep)
        f.close()
        res.append(ret)
    for r in res:
        print(r)
    slack_weights = []
    hstar_weights = []
    ff_slack_weights = []
    ff_hstar_weights = []
    for name, results in res:
        if name[0] == 'h':
            ff_slack_weights.append([float(x) for x in results[0][1][0]])
            ff_hstar_weights.append([float(x) for x in results[1][1][0]])
        else:
            slack_weights.append([float(x) for x in results[0][1][0]])
            hstar_weights.append([float(x) for x in results[1][1][0]])
    
    print("slack_weights = [")
    for w in slack_weights:
        print(f"\t{w}",)
    print("]")
    
    print("hstar_weights = [")
    for w in hstar_weights:
        print(f"\t{w}",)
    print("]")
    
    print("ff_slack_weights = [")
    for w in ff_slack_weights:
        print(f"\t{w}",)
    print("]")
    
    print("ff_hstar_weights = [")
    for w in ff_hstar_weights:
        print(f"\t{w}",)
    print("]")

if __name__ == "__main__":
    main()
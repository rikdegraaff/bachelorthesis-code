#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, math

from lab.environments import LocalEnvironment, BaselSlurmEnvironment

import common_setup
from common_setup import IssueConfig, IssueExperiment
from downward.reports.absolute import AbsoluteReport
from downward.reports.scatter import ScatterPlotReport

DIR = os.path.dirname(os.path.abspath(__file__))
BENCHMARKS_DIR = os.environ["DOWNWARD_BENCHMARKS"]
REVISIONS = ["default"]
CONFIGS = []

def times(self, num):
    return [round(num * x) for x in self]
slack_weights = [
    [25],
    [25, 24],
    [25, 2, 1],
    [25, 22, -24, -25],
    [25, 13, 12, 1, -25],
    [25, 24, 23, 22, 21, 20],
    [25, 24, -22, -23, -24, -25, -4],
    [25, 22, 21, 19, 18, 13, 12, 1],
    [25, 22, 21, 17, 16, 13, 4, 2, 1],
    [25, 23, 22, 21, 20, 17, 16, 15, 14, 1],
    [25, 20, 11, 8, 7, 4, 3, 1, -6, -5, -4],
    [25, 24, 23, 22, 21, 18, 17, 15, 7, 4, 3, 1],
]

hstar_weights = [
    [1],
    [2, 1],
    times([2.5, 1.5, 0.5],2),
    times([3.5, 2.5, 1.5, 1],2),
    times([3.5, 2.5, 1.5, 1, 0],2),
    times([4.5, 3.5, 2.5, 2, 1, 1],2),
    times([4.25, 3.25, 2.25, 1.75, 1, 0.75, -0.25],4),
    times([5.25, 4.25, 3.25, 2.75, 2, 1.75, 0.75, 1],4),
    times([5.25, 4.25, 3.25, 2.75, 2, 1.75, 0.75, 1, 0],4),
    times([5.25, 4.25, 3.25, 2.75, 2, 1.75, 0.75, 1, 1, 1],4),
    times([5.35, 4.35, 3.35, 2.75, 2, 1.75, 0.75, 1, 1, 0.8, -0.2],20),
    times([5.35, 4.35, 3.35, 2.75, 2, 1.75, 0.75, 1, 2, 1.8, 0.8, 1],20),
]

ff_slack_weights = [
    [1, 1],
    [25, 25, 24],
    [25, 25, 24, 1],
    [25, -24, -25, -23, -24],
    [1, 25, 24, 2, 1, 1],
    [25, 25, -25, -24, -25, -1, -2],
    [25, 25, -25, -24, -25, -1, -16, 8],
    times([2.5, 25, 23, 24.5, 23.5, 25, 23, 24.5, 21.5],2),
    [25, 25, 24, 25, 23, -23, -25, -1, -4, 3],
    [25, 19, -25, -1, -25, -24, -25, -1, -3, 21, -24],
    [25, 25, 22, 25, 23, 25, 1, 25, 24, 25, 0, 24],
    times([25, 25, 22, 25, 24, 25, -23.33333333, 0.66666667, -24, -23, -25, -23, -24],3),
]

ff_hstar_weights = [
    [1, 1],
    [1, 1, 0],
    [1, 1, 0, 0],
    [1, 1, 0, 0, 0],
    [1, 1, 0, 0, 0, 0],
    [1, 1, 0, 0, 0, 0, 0],
    times([1, 0.75, -0.25, -0.25, -0.25, 0, -0.25, -0.25],4),
    times([1, 0.75, -0.25, -0.25, -0.25, 0, -0.25, -0.25, 0],4),
    times([1, 0.75, -0.25, -0.25, -0.25, 0, -0.25, -0.25, 0, 0],4),
    times([1, 0.75, -0.25, -0.25, -0.25, 0, -0.25, -0.25, 0, 0, 0],4),
    times([1, 0.75, -0.25, -0.25, -0.25, 0, -0.25, -0.25, 0, 0, 0, 0],4),
    times([1, 0.75, -0.25, -0.25, -0.25, 0, -0.25, -0.25, 0, 0, 0, 0, 0],4),
]

driver_options = ["--search-time-limit", "300s"]

CONFIGS += [
    IssueConfig("blind", 
            ["--search", "eager_greedy([blind()])"],
            driver_options=driver_options,
        ),
    IssueConfig("ff", 
            ["--search", "eager_greedy([ff()])"],
            driver_options=driver_options,
        ),
]

for i in range(12):
    concepts = f"concepts(/infai/graaff/thesis/bachelorthesis-code/concepts/experiments/logistics{i}.cpt)"
    ff = "h(ff())"

    search = "eager_greedy([weighted_sum([{}],{})])"

    CONFIGS += [
        IssueConfig(f"slack{i}", 
            ["--search", search.format(concepts, str(slack_weights[i]))],
            driver_options=driver_options,
        ),
        IssueConfig(f"hstar{i}", 
            ["--search", search.format(concepts, str(hstar_weights[i]))],
            driver_options=driver_options,
        ),
        IssueConfig(f"ff_slack{i}", 
            ["--search", search.format(f"{ff},{concepts}", str(ff_slack_weights[i]))],
            driver_options=driver_options,
        ),
        IssueConfig(f"ff_hstar{i}", 
            ["--search", search.format(f"{ff},{concepts}", str(ff_hstar_weights[i]))],
            driver_options=driver_options,
        ),
    ]

SUITE = [
    'logistics00'
]

ENVIRONMENT = BaselSlurmEnvironment(
    partition="infai_1",
    email="rik.degraaff@unibas.ch",
    export=["PATH", "DOWNWARD_BENCHMARKS"])

if common_setup.is_test_run():
    SUITE = ['logistics00:probLOGISTICS-4-0.pddl']
    ENVIRONMENT = LocalEnvironment(processes=3)

exp = IssueExperiment(
    revisions=REVISIONS,
    configs=CONFIGS,
    environment=ENVIRONMENT,
)
exp.add_suite(BENCHMARKS_DIR, SUITE)

exp.add_parser(exp.EXITCODE_PARSER)
exp.add_parser(exp.SINGLE_SEARCH_PARSER)
exp.add_parser(exp.PLANNER_PARSER)
#exp.add_parser("dw_parser.py")
exp.add_parse_again_step()

exp.add_step('build', exp.build)
exp.add_step('start', exp.start_runs)
exp.add_fetcher(name='fetch')


exp.add_absolute_report_step(attributes=exp.DEFAULT_TABLE_ATTRIBUTES)

"""
exp.add_report(ScatterPlotReport(
    attributes=["initial_h_value"],
    filter_algorithm=["default_flow_no_unroll", "state_equations"]),
    outfile = os.path.join(exp.eval_dir, "scatter-flow_no_unroll-state_equations.png"))

exp.add_report(ScatterPlotReport(
    attributes=["initial_h_value"],
    filter_algorithm=["default_flow", "state_equations"]),
    outfile = os.path.join(exp.eval_dir, "scatter-flow-state_equations.png"))
"""

exp.run_steps()

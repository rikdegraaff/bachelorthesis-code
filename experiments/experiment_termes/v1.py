#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os, math

from lab.environments import LocalEnvironment, BaselSlurmEnvironment

import common_setup
from common_setup import IssueConfig, IssueExperiment
from downward.reports.absolute import AbsoluteReport
from downward.reports.scatter import ScatterPlotReport

DIR = os.path.dirname(os.path.abspath(__file__))
BENCHMARKS_DIR = os.environ["DOWNWARD_BENCHMARKS"]
REVISIONS = ["default"]
CONFIGS = []

def times(self, num):
    return [round(num * x) for x in self]

slack_weights = [
    [-7.0, 1.0],
    times([-2.18181818, 4.36363636, -25.0, 25.0],30),
    times([-1.57894737, -3.73684211, 25.0, -25.0, -10.63157895, 5.31578947, 25.0],30),
    times([-25.0, 2.21276596, -25.0, 25.0, -21.0212766, -25.0, -21.25531915, -3.42553191, -25.0, -25.0, 3.31914894, -6.9787234, -25.0],30),
    times([5.26666667, -4.4, -25.0, -14.46666667, -25.0, -11.66666667, 25.0, -19.6, -25.0, -25.0, -1.13333333, -2.26666667, 25.0, 1.0, 1.0],30),
    times([23.0, -4.75, -21.75, 25.0, -25.0, -4.5, 25.0, -20.0, -9.5, -25.0, 12.5, 25.0, 25.0, 1.0, 1.0, 3.75],4),
    times([25.0, -6.56470588, -25.0, 17.29411765, -25.0, 5.75294118, 25.0, -14.91764706, -15.47058824, -25.0, -9.12941176, -12.62352941, 25.0, 1.0, 1.0, 0.14117647, 16.48235294, 10.57647059],30),
    [25.0, -5.0, -25.0, -25.0, 1.0, 1.0]
]

hstar_weights = [
    [-1.0, 0.0],
    [0.0, -0.2, 1.4, 0.0],
    times([0.0, -0.2, 1.4, 0.0, 0.0, 1.6, 3.2],5),
    times([-1.2, -0.2, 0.0, -1.0, 0.0, -0.2, 3.4, 0.2, 0.6, 0.0, 0.2, 2.0, 0.0],5),
    times([-0.2, -0.2, 0.0, 0.0, 0.0, -0.4, 3.4, 0.2, 1.0, 0.0, 0.2, 2.2, 0.0, 1.0, 0.2],5),
    times([-0.2, -0.2, -0.2, 0.0, 0.2, 0.0, 3.6, 0.2, 1.0, 0.0, 0.2, 2.0, 0.0, 1.0, 0.2, 0.1],10),
    times([-3.9, -0.2, 0.0, -3.8, -3.7, -3.8, 0.0, 0.3, 1.1, 0.0, 0.3, 2.1, 0.0, 1.0, 0.15, 0.1, -0.1, -0.15],20),
    times([0.0, -0.2, 0.4, 0.0, 1.0, 0.3],10)
]

ff_slack_weights = [
    [1.0, -6.0, -1.0],
    times([1.0, 19.0, -0.75, 25.0, 20.25],4),
    times([1.0, -25.0, -0.5, 5.0, -24.0, -25.0, -19.5, -16.5],2),
    times([1.0, -1.66666667, -0.66666667, 25.0, -1.0, -24.0, -25.0, 11.33333333, 0.33333333, 1.0, -25.0, 1.66666667, 7.66666667, -25.0],3),
    times([1.0, 0.6, -0.6, 25.0, 0.8, -25.0, -24.2, 9.4, 0.8, 1.6, -25.0, 1.0, 5.2, -25.0, 1.0, 0.0],5),
    [1.0, 25.0, -1.0, 1.0, 25.0, 24.0, 25.0, 25.0, -2.0, 0.0, -25.0, 2.0, 8.0, 14.0, 1.0, 1.0, 0.0],
    times([1.0, 24.6875, -0.5, 25.0, 25.0, 0.625, 1.1875, 24.0625, 0.3125, 1.125, -25.0, -2.3125, -1.25, -25.0, 1.0, 1.0, 0.75, 1.375, -0.0625],16),
    [1.0, 23.0, 0.0, 23.0, 25.0, 1.0, 0.0]
]

ff_hstar_weights = [
    times([0.5, -0.2, -0.1],10),
    times([0.5, 0.16666667, -0.16666667, 1.0, 0.0],6),
    times([0.4, 0.2, -0.2, 4.0, 0.0, -2.8, -1.4, 0.0],5),
    times([0.4, 0.0, -0.2, 3.6, 0.0, -3.2, -3.4, 0.0, 0.2, 0.6, 0.0, 0.4, 2.0, 0.0],5),
    times([0.33333333, -0.1, -0.2, 3.13333333, 0.0, -3.23333333, -3.46666667, 0.0, 0.2, 0.9, 0.0, 0.3, 2.0, 0.0, 0.66666667, 0.13333333],30),
    times([0.33333333, -0.2, -0.2, 0.0, -0.13333333, 0.0, -0.13333333, 2.86666667, 0.2, 0.86666667, 0.0, 0.2, 1.73333333, 0.0, 0.66666667, 0.16666667, 0.16666667],30),
    times([0.33333333, 0.0, -0.2, 0.00952381, -0.00952381, 0.0, -0.04761905, 3.37142857, 0.14285714, 0.74285714, 0.0, 0.40952381, 2.0, 0.0, 0.66666667, 0.13333333, 0.16190476, -0.14285714, 0.0],105),
    times([0.4, 0.2, -0.2, 0.6, 0.0, 0.6, 0.2],5)
]

driver_options = ["--search-time-limit", "300s"]

CONFIGS += [
    IssueConfig("blind", 
            ["--search", "eager_greedy([blind()])"],
            driver_options=driver_options,
        ),
    IssueConfig("ff", 
            ["--search", "eager_greedy([ff()])"],
            driver_options=driver_options,
        ),
]

for i in range(8):
    concepts = "concepts(/infai/graaff/thesis/bachelorthesis-code/concepts/experiments/termes{i}.cpt)"
    ff = "h(ff())"

    search = "eager_greedy([weighted_sum([{}],{})])"

    CONFIGS += [
        IssueConfig(f"slack{i}", 
            ["--search", search.format(concepts, str(slack_weights[i]))],
            driver_options=driver_options,
        ),
        IssueConfig(f"hstar{i}", 
            ["--search", search.format(concepts, str(hstar_weights[i]))],
            driver_options=driver_options,
        ),
        IssueConfig(f"ff_slack{i}", 
            ["--search", search.format(f"{ff},{concepts}", str(ff_slack_weights[i]))],
            driver_options=driver_options,
        ),
        IssueConfig(f"ff_hstar{i}", 
            ["--search", search.format(f"{ff},{concepts}", str(ff_hstar_weights[i]))],
            driver_options=driver_options,
        ),
    ]

SUITE = [
    'termes-opt18-strips',
    'termes-sat18-strips'
]

ENVIRONMENT = BaselSlurmEnvironment(
    partition="infai_2",
    email="rik.degraaff@unibas.ch",
    export=["PATH", "DOWNWARD_BENCHMARKS"])

if common_setup.is_test_run():
    SUITE = ['logistics00:probLOGISTICS-4-0']
    ENVIRONMENT = LocalEnvironment(processes=3)

exp = IssueExperiment(
    revisions=REVISIONS,
    configs=CONFIGS,
    environment=ENVIRONMENT,
)
exp.add_suite(BENCHMARKS_DIR, SUITE)

exp.add_parser(exp.EXITCODE_PARSER)
exp.add_parser(exp.SINGLE_SEARCH_PARSER)
exp.add_parser(exp.PLANNER_PARSER)
#exp.add_parser("dw_parser.py")
exp.add_parse_again_step()

exp.add_step('build', exp.build)
exp.add_step('start', exp.start_runs)
exp.add_fetcher(name='fetch')


exp.add_absolute_report_step(attributes=exp.DEFAULT_TABLE_ATTRIBUTES)

"""
exp.add_report(ScatterPlotReport(
    attributes=["initial_h_value"],
    filter_algorithm=["default_flow_no_unroll", "state_equations"]),
    outfile = os.path.join(exp.eval_dir, "scatter-flow_no_unroll-state_equations.png"))

exp.add_report(ScatterPlotReport(
    attributes=["initial_h_value"],
    filter_algorithm=["default_flow", "state_equations"]),
    outfile = os.path.join(exp.eval_dir, "scatter-flow-state_equations.png"))
"""

exp.run_steps()

#! /usr/bin/env python3

import cplex
import sys
import os

from driver.main import main as fast_downward
from driver.optimizer.arguments import parse_arguments
from driver.optimizer.state_space import parse_state_space, State
from driver.optimizer.optimize import optimize
from importlib import import_module

def main():
    args = parse_arguments()
    samples_paths = f"{args['samples-path']}/{args['domain']}"
    if "sample" in args['steps'] or "evaluate" in args['steps']:
        if not os.path.exists(samples_paths):
            os.makedirs(samples_paths)
        sample_all(f"{args['benchmarks-path']}/{args['domain']}", 
            args['tasks'], 
            samples_paths, 
            args['evaluators'], 
            not "sample" in args['steps'])
    if "optimize" in args['steps']:
        renderer_path = args.get('renderer')
        if renderer_path is not None:
            renderer = getattr(import_module(renderer_path), 'render')
        else:
            renderer = State.__str__

        limit = args.get('limit')
        if limit is not None:
            limit = int(limit)

        tasks = args['tasks']

        sample_rates = args.get('sample-rates')
        sample_rates = [float(rate) for rate in args['sample-rates']]
        if len(sample_rates) != len(tasks):
            if len(sample_rates) == 1:
                sample_rates = sample_rates*len(tasks)
            else:
                print(f"specified {len(tasks)} tasks and {len(sample_rates)} sample rates.")
                exit()
        res = optimize_variants(samples_paths, tasks, args['methods'], sample_rates, renderer, limit)
        return args['evaluators'], res

def sample_all(domain_path, tasks, output_path, evaluators, evaluate_only):
    for task in tasks:
        fast_downward_search(f"{domain_path}/{task}.pddl", 
            f"sample([{','.join(evaluators)}],{output_path}/{task},{not evaluate_only})", 
            True, [12])

def optimize_variants(samples_path, tasks, methods, sample_rates, renderer, limit):
    files = map(lambda task: (task[0], f"{samples_path}/{task[0]}.ss", f"{samples_path}/{task[0]}.pf", task[1]), zip(tasks, sample_rates))
    state_spaces = list(map(lambda fs: parse_state_space(*fs), files))
    
    complexities = [1]*len(state_spaces[0].get_state(0).features)
    res = []
    for method in methods:
        res.append(optimize(state_spaces, complexities, 25, method, renderer, limit))
    return res

def fast_downward_search(task, search, keep_unreachable, acceptable_exitcodes=None):
    def fake_exit(exitcode):
        if acceptable_exitcodes is None or exitcode in acceptable_exitcodes:
            return
        exit(exitcode)
    ext = sys.exit
    sys.exit = fake_exit
    argv = sys.argv.copy()
    sys.argv = sys.argv[:1] + [task, "--search", search]
    if keep_unreachable:
        sys.argv += [ "--translate-options", "--keep-unreachable-facts"]
    fast_downward()
    sys.exit = ext
    sys.argv = argv

if __name__ == "__main__":
    main()
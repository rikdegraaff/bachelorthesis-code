import cplex
import math
import heapq

def x_plus(f):
    return f"x_plus_{f}"

def x_minus(f):
    return f"x_minus_{f}"

def weight_var(f):
    return f"w_{f}"

def y_var(state_space_index, state, succ):
    return f"y_{state_space_index}_{state.id}_{succ.id}"

def weight_constraint_name(state_space_index, state, succ, negated=False):
    return f"c_{state_space_index}_{state.id}_{succ.id}{'_' if negated else ''}"

def dead_constraint_name(state_space_index, state, succ, other=None):
    if other is None:
        return f"d_{state_space_index}_{state.id}_{succ.id}"
    return f"{dead_constraint_name(state_space_index, state, succ)}_{other.id}"

def y_constraint(state_space_index, state):
    return f"c_y_{state_space_index}_{state.id}"

def u_slack(state_space_index, state, succ=None):
    if succ is None:
        return f"u_{state_space_index}_{state.id}"
    return f"{u_slack(state_space_index, state)}_{succ.id}"

def v_slack(state_space_index, state, succ):
    return f"v_{state_space_index}_{state.id}_{succ.id}"

def optimize(state_spaces, feature_complexities, max_weight, method, renderer, limit):
    if method in ["strict", "slack"]:
        res = optimize_descending(state_spaces, feature_complexities, max_weight, method == "strict", renderer, limit)
    elif method == "hstar":
        res = optimize_h_star(state_spaces, len(feature_complexities), renderer, limit)
    return method, res

def optimize_descending(state_spaces, feature_complexities, max_weight, strict, renderer, limit):
    print("Populating model")
    problem = cplex.Cplex()
    if limit is not None:
        problem.parameters.timelimit.set(limit)
    problem.objective.set_sense(problem.objective.sense.minimize)

    print("Populating objective function")
    populate_obj_function(problem, max_weight, state_spaces, feature_complexities, strict)
    print("Populating weight constraints")
    populate_weight_constraints(problem, state_spaces, strict)
    print("Populating dead-end constraints")
    populate_dead_end_constraints(problem, state_spaces, strict)
    print("Populating y-constraints")
    populate_y_constraints(problem, state_spaces)
    if strict:
        print("Populating M_w-constraints")
        populate_max_weight_constraints(problem, len(feature_complexities), max_weight)

    print("Writing file...")
    problem.write("temp.lp")
    print("Total number of constraints in the MIP: ", problem.linear_constraints.get_num() + problem.indicator_constraints.get_num())
    print("Total number of variables in the MIP: ", problem.variables.get_num())
    print("Solving MIP...")

    # Below, some ways to limit resources. This allows suboptimal solutions (or no solution).
    # To set the timelimit:
    # problem.parameters.timelimit.set(100)
    # To set the limit of solutions found:
    # problem.parameters.mip.limits.solutions.set(10)
    # To set high numerical precision on, turn on the following:
    # problem.parameters.emphasis.numerical.set(1)

    problem.solve()
    if problem.solution.is_primal_feasible():
        if not problem.solution.is_dual_feasible():
            print("MIP might be unbounded")
        print("Optimal solution found with value {}".format(problem.solution.get_objective_value()))
        
        non_zero_weights, all_weights = extract_heuristic_parameters_from_cplex_solution(problem, len(feature_complexities))

        if not strict:
            report_highest_u_slack(problem, state_spaces, False, all_weights, renderer)
            report_highest_v_slack(problem, state_spaces, False, all_weights, renderer)

        print()
        print(f"feature weights are:")
        for f, w in non_zero_weights:
            print(f"{f}: {state_spaces[0].feature_names[f]}")
            print("weight:", w)
        return all_weights, problem.solution.get_objective_value()        
    elif problem.solution.is_dual_feasible():
        print("MIP is unsolvable")
    else:
        print("MIP was not solved. Unknown reason.")
    return [], math.inf

def optimize_h_star(state_spaces, num_features, renderer, limit):
    print("Populating model")
    problem = cplex.Cplex()
    if limit is not None:
        problem.parameters.timelimit.set(limit)
    problem.objective.set_sense(problem.objective.sense.minimize)

    print("Populating objective function")
    populate_h_star_obj_function(problem, state_spaces, num_features)
    print("Populating weight constraints")
    populate_h_star_weight_constraints(problem, state_spaces)
    print("Populating dead-end constraints")
    populate_h_star_dead_end_constraints(problem, state_spaces)
    
    #print("Writing file...")
    #problem.write("temp.lp")
    print("Total number of constraints in the MIP: ", problem.linear_constraints.get_num() + problem.indicator_constraints.get_num())
    print("Total number of variables in the MIP: ", problem.variables.get_num())
    print("Solving MIP...")

    # Below, some ways to limit resources. This allows suboptimal solutions (or no solution).
    # To set the timelimit:
    # problem.parameters.timelimit.set(100)
    # To set the limit of solutions found:
    # problem.parameters.mip.limits.solutions.set(10)
    # To set high numerical precision on, turn on the following:
    # problem.parameters.emphasis.numerical.set(1)

    problem.solve()
    if problem.solution.is_primal_feasible() and problem.solution.is_dual_feasible():
        print("Optimal solution found with value {}".format(problem.solution.get_objective_value()))

        non_zero_weights, all_weights = extract_heuristic_parameters_from_cplex_solution(problem, num_features)

        report_highest_u_slack(problem, state_spaces, True, all_weights, renderer)
        report_highest_v_slack(problem, state_spaces, True, all_weights, renderer)

        print()
        print(f"feature weights are:")
        for f, w in non_zero_weights:
            print(f"{f}: {state_spaces[0].feature_names[f]}")
            print("weight:", w)
        return all_weights, problem.solution.get_objective_value()        
    elif problem.solution.is_primal_feasible():
        print("MIP is unbounded")
    elif problem.solution.is_dual_feasible():
        print("MIP is unsolvable")
    else:
        print("MIP was not solved. Unknown reason.")
    return [], math.inf

def populate_obj_function(problem, max_weight, state_spaces, feature_complexity, strict):
    num_features = len(feature_complexity)

    if strict:
        for f in range(0, len(feature_complexity)):
            problem.variables.add(names=[x_plus(f), x_minus(f)],
                                  obj=[feature_complexity[f], feature_complexity[f]],
                                  types=[problem.variables.type.binary] * 2)

    problem.variables.add(names=[weight_var(f) for f in range(num_features)],
                          obj=[0] * num_features,
                          lb=[-1 * max_weight] * num_features, ub=[max_weight] * num_features,
                          types=[problem.variables.type.continuous] * num_features)

    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            successors = list(state.get_successors())
            problem.variables.add(names=[y_var(state_space_index, state, succ) for succ in successors],
                                  obj=[0] * len(successors),
                                  types=[problem.variables.type.binary] * len(successors))
            if not strict:
                solvable_successors = [s for s in successors if s.solvable()]
                unsolvable_successors = [s for s in successors if s.unsolvable()]
                if len(solvable_successors) > 0:
                    problem.variables.add(names=[u_slack(state_space_index, state)],
                                          obj=[1],
                                          lb=[0],
                                          types=[problem.variables.type.continuous])
                if len(unsolvable_successors) > 0:
                    problem.variables.add(names=[v_slack(state_space_index, state, succ) for succ in unsolvable_successors],
                                          obj=[1] * len(unsolvable_successors),
                                          lb=[0] * len(unsolvable_successors),
                                          types=[problem.variables.type.continuous] * len(unsolvable_successors))

    problem.variables.add(names=["dummy"], obj=[0], lb=[1], ub=[1], types=[problem.variables.type.integer])

def populate_h_star_obj_function(problem, state_spaces, num_features):
    problem.variables.add(names=[weight_var(f) for f in range(num_features)],
                          obj=[0] * num_features,
                          lb=[-1 * cplex.infinity] * num_features, ub=[cplex.infinity] * num_features,
                          types=[problem.variables.type.continuous] * num_features)
    
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            successors = list(state.get_successors())            
            solvable_successors = [s for s in successors if s.solvable()]
            unsolvable_successors = [s for s in successors if s.unsolvable()]
            problem.variables.add(names=[u_slack(state_space_index, state, succ) for succ in solvable_successors],
                                  obj=[1] * len(solvable_successors),
                                  lb=[0] * len(solvable_successors),
                                  types=[problem.variables.type.continuous] * len(solvable_successors))
            problem.variables.add(names=[v_slack(state_space_index, state, succ) for succ in unsolvable_successors],
                                  obj=[1] * len(unsolvable_successors),
                                  lb=[0] * len(unsolvable_successors),
                                  types=[problem.variables.type.continuous] * len(unsolvable_successors))
    problem.variables.add(names=["factor"], obj=[0], lb=[1], types=[problem.variables.type.continuous])

def populate_weight_constraints(problem, state_spaces, strict):
    lin_expr = []
    names = []
    ind_vars = []
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            for succ in filter(lambda s: s.solvable(), state.get_successors()):
                coefficients = []
                var_names = []

                if not strict:
                    var_names.append(u_slack(state_space_index, state))
                    coefficients.append(1)

                features_start = state.features
                features_final = succ.features

                # Add w_f*([f]^s - [f]^s') to every feature
                for f, (start_f, final_f) in enumerate(zip(features_start, features_final)):
                    delta_f = start_f - final_f
                    coefficients.append(delta_f)
                    var_names.append(weight_var(f))

                lin_expr.append([var_names, coefficients])
                names.append(weight_constraint_name(state_space_index, state, succ))
                ind_vars.append(y_var(state_space_index, state, succ))

    problem.indicator_constraints.add_batch(
        indvar=ind_vars,
        lin_expr=lin_expr,
        sense=["G"] * len(lin_expr),
        rhs=[1] * len(lin_expr),
        name=names)

def populate_h_star_weight_constraints(problem, state_spaces):
    lin_expr = []
    names = []
    rhs = []
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            for succ in filter(lambda s: s.solvable(), state.get_successors()):
                coefficients = []
                neg_coefficients = []
                var_names = []

                var_names.append(u_slack(state_space_index, state, succ))
                coefficients.append(1)
                neg_coefficients.append(1)

                var_names.append("factor")
                coefficients.append(succ.h_star - state.h_star)
                neg_coefficients.append(state.h_star - succ.h_star)

                features_start = state.features
                features_final = succ.features

                # Add w_f*([f]^s - [f]^s') to every feature
                for f, (start_f, final_f) in enumerate(zip(features_start, features_final)):
                    delta_f = start_f - final_f
                    coefficients.append(delta_f)
                    neg_coefficients.append(-delta_f)
                    var_names.append(weight_var(f))

                lin_expr.append([var_names, coefficients])
                rhs.append(state.h_star - succ.h_star)
                names.append(weight_constraint_name(state_space_index, state, succ))

                lin_expr.append([var_names, neg_coefficients])
                rhs.append(succ.h_star - state.h_star)
                names.append(weight_constraint_name(state_space_index, state, succ, True))

    problem.linear_constraints.add(
        lin_expr=lin_expr,
        senses=["G"] * len(lin_expr),
        rhs=[0] * len(lin_expr),
        names=names)

def populate_dead_end_constraints(problem, state_spaces, strict):
    lin_expr = []
    names = []
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            for succ in filter(lambda s: s.unsolvable(), state.get_successors()):
                coefficients = []
                var_names = []

                if not strict:
                    var_names.append(v_slack(state_space_index, state, succ))
                    coefficients.append(-1)

                features_start = state.features
                features_final = succ.features

                for f, (start_f, final_f) in enumerate(zip(features_start, features_final)):
                    delta_f = start_f - final_f
                    coefficients.append(delta_f)
                    var_names.append(weight_var(f))

                lin_expr.append([var_names, coefficients])
                names.append(dead_constraint_name(state_space_index, state, succ))

    problem.linear_constraints.add(
        lin_expr=lin_expr,
        senses=["L"] * len(lin_expr),
        rhs=[0] * len(lin_expr),
        names=names)

def populate_h_star_dead_end_constraints(problem, state_spaces):
    lin_expr = []
    names = []
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            for succ in filter(lambda s: s.unsolvable(), state.get_successors()):
                for other in ([s for s in state.get_successors() if s.solvable()] + [state]):
                    coefficients = []
                    var_names = []

                    var_names.append(v_slack(state_space_index, state, succ))
                    coefficients.append(-1)

                    features_start = other.features
                    features_final = succ.features

                    for f, (start_f, final_f) in enumerate(zip(features_start, features_final)):
                        delta_f = start_f - final_f
                        coefficients.append(delta_f)
                        var_names.append(weight_var(f))

                    lin_expr.append([var_names, coefficients])
                    names.append(dead_constraint_name(state_space_index, state, succ, other))

    problem.linear_constraints.add(
        lin_expr=lin_expr,
        senses=["L"] * len(lin_expr),
        rhs=[0] * len(lin_expr),
        names=names)

def populate_y_constraints(problem, state_spaces):
    lin_expr = []
    names = []
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            successors = [s for s in state.get_successors() if not s.unsolvable()]
            if state.goal() or state.unsolvable() or len(successors) == 0:
                continue
            names.append(y_constraint(state_space_index, state))
            y_vars = []
            for final in successors:
                y_vars.append(y_var(state_space_index, state, final))
            lin_expr.append([y_vars, [1] * len(y_vars)])
    
    problem.linear_constraints.add(lin_expr=lin_expr, 
                                   senses=["G"] * len(lin_expr), 
                                   rhs=[1] * len(lin_expr),
                                   names=names)

def populate_max_weight_constraints(problem, num_features, max_weight):
    coefficients = []
    for f in range(0, num_features):
        w_name = weight_var(f)

        # x_+ constraint
        xplus_name = 'c_xplus_' + str(f)
        problem.linear_constraints.add(names=[xplus_name])
        coefficients.append((xplus_name, w_name, -1))
        coefficients.append((xplus_name, x_plus(f), max_weight))
        problem.linear_constraints.set_rhs(xplus_name, 0)
        problem.linear_constraints.set_senses(xplus_name, "G")

        # x_- constraint
        xminus_name = 'c_xminus_' + str(f)
        problem.linear_constraints.add(names=[xminus_name])
        coefficients.append((xminus_name, w_name, 1))
        coefficients.append((xminus_name, x_minus(f), max_weight))
        problem.linear_constraints.set_rhs(xminus_name, 0)
        problem.linear_constraints.set_senses(xminus_name, "G")

        # w_f <= max_weight constraint
        """c_max_weight_name = "c_max_w_" + w_name
        problem.linear_constraints.add(names=[c_max_weight_name])
        coefficients.append((c_max_weight_name, w_name, 1))
        coefficients.append((c_max_weight_name, "dummy", -1 * max_weight))
        problem.linear_constraints.set_rhs(c_max_weight_name, 0)
        problem.linear_constraints.set_senses(c_max_weight_name, "L")"""
    problem.linear_constraints.set_coefficients(coefficients)
# Cplex sometimes returns float values even if the variable is declared as an integer
def make_cplex_number_rounded(x):
    rounded_number = round(x,8)
    integer_number = int(round(x))
    if math.isclose(rounded_number, integer_number, abs_tol=0.00000009):
        return integer_number
    else:
        return rounded_number

def extract_heuristic_parameters_from_cplex_solution(problem, n_features):
    #make_weight_rounded = lambda x: x

    w_var_names = ((i, weight_var(i)) for i in range(n_features))
    feature_weights = [(i, w_name, make_cplex_number_rounded(problem.solution.get_values(w_name))) for i, w_name in w_var_names]
    nonzero_features = [(i, val) for i, var, val in feature_weights if val != 0]
    all_weights = [val for i, var, val in feature_weights]
    return nonzero_features, all_weights

def report_highest_u_slack(problem, state_spaces, specific_successor, weights, renderer):
    slacks = []
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            if specific_successor:
                for succ, op in filter(lambda s: s[0].solvable(), state.get_successors_and_ops()):
                    slacks.append(((state, (succ, op)), u_slack(state_space_index, state, succ)))
            else:
                slacks.append((state, u_slack(state_space_index, state)))
    states_and_slacks = [(state_and_slack[0], problem.solution.get_values(state_and_slack[1])) for state_and_slack in slacks]
    worst = heapq.nlargest(5, states_and_slacks, lambda state_and_slack: state_and_slack[1])
    for s, slack in worst:
        if slack == 0:
            continue
        state = s[0] if type(s) == tuple else s
        successors = [s[1]] if type(s) == tuple else [s for s in state.get_successors_and_ops() if s[0].solvable()]
        print()
        print("state in task", state.state_space.name)
        print("slack:", make_cplex_number_rounded(slack))
        print("values:")
        print(renderer(state))
        print("features:", state.features)
        print("h:", make_cplex_number_rounded(state.h(weights)))
        print("h*:", state.h_star)

        print(f"successor{'s' if len(successors) > 1 else ''}")
        for successor, op in successors:
            print("operator:", op)
            print("values:")
            print(renderer(successor))
            print("features:", successor.features)
            print("h:", make_cplex_number_rounded(successor.h(weights)))
            print("h*", successor.h_star)

def report_highest_v_slack(problem, state_spaces, show_siblings, weights, renderer):
    slacks = []
    for state_space_index, state_space in enumerate(state_spaces):
        for state in state_space.get_states():
            if state.goal() or state.unsolvable():
                continue
            for succ in filter(lambda p: p[0].unsolvable(), state.get_successors_and_ops()):
                slacks.append(((state, succ), v_slack(state_space_index, state, succ[0])))
    states_and_slacks = [(state_and_slack[0], problem.solution.get_values(state_and_slack[1])) for state_and_slack in slacks]
    worst = heapq.nlargest(5, states_and_slacks, lambda state_and_slack: state_and_slack[1])
    for (state, (succ, operator)), slack in worst:
        if slack == 0:
            continue
        print()
        print("unsolvable state in task", succ.state_space.name)
        print("operator:", operator)
        print("slack:", make_cplex_number_rounded(slack))
        print("values:")
        print(renderer(succ))
        print("features:", succ.features)
        print("h:", make_cplex_number_rounded(succ.h(weights)))

        print()
        print("predecessor")
        print("values:")
        print(renderer(state))
        print("features:", state.features)
        print("h:", make_cplex_number_rounded(state.h(weights)))
        print("h*:", state.h_star)

        if show_siblings:
            siblings = [(s, op) for s, op in state.get_successors_and_ops() if s.solvable()]
            print(f"siblings{'s' if len(siblings) > 1 else ''}")
            for sibling, op in siblings:
                print("operator:", op)
                print("values:")
                print(renderer(sibling))
                print("features:", sibling.features)
                print("h:", make_cplex_number_rounded(sibling.h(weights)))
                print("h*", sibling.h_star)
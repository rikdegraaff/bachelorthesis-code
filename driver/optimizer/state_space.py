import os
from random import random

class State (object):
    def __init__(self, state_space, values, h_star, features, successors, id, skip):
        self.state_space = state_space
        self.values = values
        self.h_star = h_star
        self.features = features
        self._successors = successors
        self.id = id
        self._skip = skip
    
    def get_successors(self):
        for s, _ in self._successors:
            yield self.state_space.get_state(s)
    
    def get_successors_and_ops(self):
        for s, op in self._successors:
            yield self.state_space.get_state(s), self.state_space.get_op(op)

    def unsolvable(self):
        return self.h_star == -1

    def solvable(self):
        return not self.unsolvable()

    def goal(self):
        return self.h_star == 0

    def h(self, weights):
        return sum([f * w for f, w in zip(self.features, weights)])

    def get_facts(self):
        facts = {}
        for i in range(len(self.values)):
            fact = self.state_space.variables[i][self.values[i]]
            if fact is None:
                continue
            facts.setdefault(fact.predicate, []).append(fact.individuals)
        return facts

    def __str__(self):
        facts = self.get_facts()

        height = 0
        for k in facts:
            height = max(len(facts[k]), height)
        
        col_widths = [0]*len(facts)

        lst = [[] for _ in range(height + 1)]
        for col, k in enumerate(sorted(facts)):
            fact = facts[k]
            lst[0].append(k)
            col_widths[col] = max(col_widths[col], len(k))
            values = sorted([val[0] if len(val) == 1 else '(' + ', '.join(val) + ')' for val in fact])
            for i in range(height):
                if i < len(values):
                    val = values[i]
                    lst[i+1].append(val)
                    col_widths[col] = max(col_widths[col], len(val))
                else:
                    lst[i+1].append("")

        table = ""
        for i, row in enumerate(lst):
            if i != 0:
                table += os.linesep
            for col, cell in enumerate(row):
                table += cell.ljust(col_widths[col]+2)

        return table

class Fact (object):
    def __init__(self, s):
        parts = s.split(' ')
        self.predicate = parts[0]
        self.individuals = parts[1:]

class StateSpace (object):
    def __init__(self, name, individuals, operators, variables, states, h_star, features, successors, skip, feature_names):
        self.name = name
        self._states = []
        for i in range(len(states)):
            self.individuals = individuals
            self.operators = operators
            self.variables = variables
            self._states.append(State(self, states[i], h_star[i], features[i], successors[i], i, skip[i]))
            self.feature_names = feature_names
    
    def get_state(self, index):
        return self._states[index]
    
    def get_op(self, index):
        return self.operators[index]
    
    def get_states(self):
        for s in self._states:
            if not s._skip:
                yield s

def parse_state_space(name, state_space_file_path, features_file_path, sample_rate):
    states = []
    h_star = []
    features = []
    successors = []
    skip = []

    print(name, state_space_file_path, features_file_path)

    state_space_file = open(state_space_file_path, "r")
    features_file = open(features_file_path, "r")

    first_ss_line = next(state_space_file).strip()
    parts = first_ss_line.split(';')
    individuals = parts[0].split(',')

    second_ss_line = next(state_space_file).strip()
    operators = second_ss_line.split(',')

    variables = []
    for variable in parts[1:]:
        variables.append([Fact(fact) if fact.strip() != "" else None for fact in variable.split(',')])   

    feature_names = next(features_file).strip().split(';')

    for state_space_line, features_line in zip(state_space_file, features_file):
        if "" in [state_space_line, features_line]:
            continue

        features.append(comma_separated_to_int_list(features_line))
        [state, h_s, succs] = nested_separated_to_int_lists(state_space_line)
        states.append(state)
        h_star.append(h_s[0])
        successors.append(succs)
        skip.append(random() >= sample_rate)

    return StateSpace(name, individuals, operators, variables, states, h_star, features, successors, skip, feature_names)

def comma_separated_to_int_list(csv):
    def to_int_or_list(s):
        if '|' in s:
            return list(map(int, s.split("|")))
        else:
            return int(s)
    return list(map(to_int_or_list, csv.split(",")))

def nested_separated_to_int_lists(nsv):
    return list(map(comma_separated_to_int_list, nsv.split(";")))
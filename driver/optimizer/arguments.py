import os
import sys

def parse_arguments():
    arguments = [
        Argument("benchmarks-path", default=os.getenv("DOWNWARD_BENCHMARKS")),
        Argument("domain"),
        Argument("samples-path"),
        Argument("tasks", lst=True),
        Argument("evaluators", lst=True),
        Argument("methods", lst=True, valid=method_valid),
        Argument("sample-rates", lst=True, valid=is_rate, default=["1"]),
        Argument("renderer"),
        Argument("limit", valid=is_natural),
        Argument("steps", lst=True, depends=["domain", "samples-path", "tasks"], valid=step_valid, required=True)
    ]

    args = sys.argv[1:]
    found = []
    result = {}
    while len(args) > 0:
        matched = False
        for argument in arguments:
            if (not argument.name in found or argument.used_default) and argument.parse(args, found):
                matched = True
                found.append(argument.name)
                result[argument.name] = argument.value
        if not matched:
            print(f"unknown argument '{args[0]}'")
            exit()
    
    ok = True
    for argument in arguments:
        if argument.required and not argument.name in found:
            ok = False
            print(f"argument '--{argument.name}' is required")
    if not ok:
        exit()

    return result

def step_valid(step, found):
    if step in ["sample", "evaluate"]:
        valid = True
        if not "benchmarks-path" in found:
            valid = False
            print(f"'{step}' step requires '--benchmarks-path'")
        return valid
    if step == "optimize":
        valid = True
        if not "methods" in found:
            valid = False
            print(f"'{step}' step requires '--methods'")
        return valid
    print(f"unknown step '{step}'")
    return False

def is_natural(number, found):
    try:
        num = int(number)
    except ValueError:
        return False
    return 0 <= num

def is_rate(number, found):
    try:
        rate = float(number)
    except ValueError:
        return False
    return 0 <= rate <= 1


def method_valid(method, found):
    if method in ["strict", "slack", "hstar"]:
        return True
    print(f"unknown optimization method '{method}'")
    return False

class Argument (object):
    def __init__(self, name, lst=False, depends=[], default=None, valid=None, required=False):
        self.name = name
        self.lst = lst
        self.value = [] if lst else None
        self.depends = depends
        self.default = default
        self.valid = valid
        self.required = required
        self.used_default = False
    
    def check(self, argument, found):
        if self.valid is None or self.valid(argument, found):
            return
        exit()

    def parse(self, args, found):
        if len(args) > 0 and args[0] == "--" + self.name:
            self.used_default = False
            args.pop(0)
            valid = True
            for dependency in self.depends:
                if not dependency in found:
                    valid = False
                    print(f"'--{self.name}''' depends on '--{dependency}")
            if not valid:
                exit()
            if len(args) == 0:
                print(f"expected value after '--{self.name}'")
                exit()
            if args[0].startswith("--"):
                print(f"expected value after '--{self.name}' but found '{args[0]}'")
                exit()
            if not self.lst:
                current = args.pop(0)
                self.check(current, found)
                self.value = current
                return True
            self.value = []
            while len(args) > 0 and not args[0].startswith("--"):
                current = args.pop(0)              
                self.check(current, found)
                self.value.append(current)
            return True
        if self.default is not None:
            self.value = self.default
            self.used_default = True
            return True
        return False
